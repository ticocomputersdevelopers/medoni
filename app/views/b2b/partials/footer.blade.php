<footer>
    <div class="container">
        <div class="row"> 
            <div class="col-md-3 col-sm-3 col-xs-12">    
<!--                 <a class="logo" href="{{ B2bOptions::base_url()}}b2b" title="{{B2bOptions::company_name()}}">
                    <img class="img-responsive" src="{{ B2bOptions::base_url()}}{{B2bOptions::company_logo()}}" alt="{{B2bOptions::company_name()}}" />
                </a> -->
                <ul class="footer-lists">
                    @foreach (B2bCommon::menu_footer() as $row)
                    <li>
                        <a class="inline-block" href="{{ B2bOptions::base_url().'b2b/'.$row->naziv_stranice }}">{{ $row->title }}</a>
                    </li>
                    @endforeach
                </ul> 
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 text-center">  
                <div class="relative newsletter-input inline-block"> 
                    {{-- @if(Options::newsletter()==1) --}}
                    <input type="text" class="" placeholder="Vaša E-mail Adresa" id="newsletter" />
                    <button class="button" onclick="newsletter()">Prijavite se</button>
                    {{-- @endif --}}
                </div>  
                <div class="social-icons"> 
                    {{Options::social_icon()}}
                </div>
            </div>  

            <div class="col-md-3 col-sm-3 col-xs-12">     
                <ul> 
                    <li>
                        <h4>Kontakt 
                            @if(Options::company_map() != '' && Options::company_map() != ';') 
                            <span class="JStoggMap inline-block pointer relative">Prikaži mapu <i class="fas fa-map-marker-alt"></i></span>
                            @endif 
                            </h4>  
                        </li>
                    <li>
                        {{ Options::company_adress() }}
                        {{ Options::company_city() }}  
                    </li>

                    <li>{{ Options::company_phone() }}</li> 
                    <li>{{ Options::company_fax() }}</li> 
                        
                    <li> 
                        <a href="mailto:{{ Options::company_email() }}"> {{ Options::company_email() }} </a>
                    </li> 
                </ul>   
            </div>  
        </div>  
    </div>  

    @if(Options::company_map() != '' && Options::company_map() != ';')
    <div class="map-frame JSmap relative">
        
        <div class="map-info">
            <h5>{{ Options::company_name() }}</h5>
            <h6>{{ Options::company_adress() }}, {{ Options::company_city() }}</h6> 
        </div>

        <iframe src="https://maps.google.com/maps?q={{ All::lat_long()[0] }},{{ All::lat_long()[1] }}&output=embed" width="100%" height="100%" frameborder="0" aria-hidden="false" tabindex="0"></iframe> 

    </div> 
    @endif
    
    <div class="container">
        <div class="text-center foot-note"> 
            <span>{{B2bOptions::company_name() }}  {{ date('Y') }}. Sva prava zadržana <i class="far fa-copyright"></i></span>
            <a href="https://www.selltico.com/" target="_blank">- Izrada B2B portala</a>  
            <a href="https://www.selltico.com/" target="_blank">- Selltico.</a> 
        </div>
    </div>

    <a class="JSscroll-top" href="javascript:void(0)">
        <i class="fa fa-angle-up"></i>
    </a> 
</footer>


