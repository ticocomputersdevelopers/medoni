<div id="main-content" class="images-edit">

	<h2 class="text-center section-heading">{{ AdminLanguage::transAdmin('Učitane slike') }}</h2>

	<div class="row firma-slike-row">
		@foreach($images as $image)
			<div class="column large-2 medium-6 small-12 col-slika">
				<img src="{{ AdminOptions::base_url().'images/upload_image/'.$image }}">
				<a class="btn btn-danger" href="{{ AdminOptions::base_url().'admin/upload-image-delete/'.$image }}">{{ AdminLanguage::transAdmin('Obriši') }}</a>
			</div>
		@endforeach
			<div class="column large-2 medium-6 small-12 col-slika">
				<form method="POST" action="{{AdminOptions::base_url()}}admin/upload-image-add" enctype="multipart/form-data" id="JSUploadForm">
					<div class="field-group text-center {{ $errors->first('slika') ? 'error' : '' }}">
						<div class="bg-image-file"> 
							<input type="file" name="slika" id="JSSlika"> 
						</div>
					 </div>
				</form>
			</div>
	</div>
</div>
