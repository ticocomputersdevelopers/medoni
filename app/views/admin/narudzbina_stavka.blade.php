<section id="main-content">

	<div class="m-subnav"> 
		<a class="m-subnav__link" href="{{AdminOptions::base_url()}}admin/narudzbina/{{$narudzbina_stavka->web_b2c_narudzbina_id}}">
			<div class="m-subnav__link__icon">
				<i class="fa fa-arrow-left" aria-hidden="true"></i>
			</div>
			<div class="m-subnav__link__text">
				{{ AdminLanguage::transAdmin('Nazad') }}
			</div>
		</a> 
	</div>

	<form method="POST" action="{{AdminOptions::base_url()}}admin/narudzbina-stavka">
		<input type="hidden" name="web_b2c_narudzbina_stavka_id" value="{{ $narudzbina_stavka->web_b2c_narudzbina_stavka_id }}">

		<div class="flat-box padding-h-8"> 
			<div>{{ AdminLanguage::transAdmin('Stavka') }}: {{ $ime_stavke }}</div>
			<div>{{ AdminLanguage::transAdmin('Količina') }}: {{intval($narudzbina_stavka->kolicina)}}</div>
		</div>

		<div class="flat-box"> 
			<div class="row"> 
				<div class="columns medium-3">  
					<label>{{ AdminLanguage::transAdmin('Pošiljalac') }}:</label>
					<select name="posiljalac_id">
						@foreach(AdminPartneri::partneri() as $partner)
						@if((!is_null(Input::old('posiljalac_id')) ? Input::old('posiljalac_id') : $narudzbina_stavka->posiljalac_id) == $partner->partner_id)
						<option value="{{$partner->partner_id}}" selected>{{$partner->naziv}}</option>
						@else
						<option value="{{$partner->partner_id}}">{{$partner->naziv}}</option>
						@endif
						@endforeach
					</select> 
				</div>

				<div class="columns medium-3"> 
					<label>{{ AdminLanguage::transAdmin('Pošta') }}:</label>
					<select name="posta_slanje_id">
						@foreach(AdminPartneri::poste() as $posta_slanje)
						@if((!is_null(Input::old('posta_slanje_id')) ? Input::old('posta_slanje_id') : $narudzbina_stavka->posta_slanje_id) == $posta_slanje->posta_slanje_id)
						<option value="{{$posta_slanje->posta_slanje_id}}" selected>{{$posta_slanje->naziv}}</option>
						@else
						<option value="{{$posta_slanje->posta_slanje_id}}">{{$posta_slanje->naziv}}</option>
						@endif
						@endforeach
					</select> 
				</div> 

				<div class="columns medium-3">
					<label>{{ AdminLanguage::transAdmin('Opština') }}</label>				 
					<select class="form-control" name="opstina" id="JSOpstina" tabindex="6">
						@foreach(AdminNarudzbine::opstine() as $key => $opstina)
						@if((Input::old('opstina') ? Input::old('opstina') : AdminNarudzbine::opstina_id($narudzbina_stavka->ulica_id)) == $opstina->narudzbina_opstina_id)
						<option value="{{ $opstina->narudzbina_opstina_id }}" selected>{{ $opstina->naziv }}</option>
						@else
						<option value="{{ $opstina->narudzbina_opstina_id }}">{{ $opstina->naziv }}</option>
						@endif
						@endforeach
					</select>
				</div>

				<div class="columns medium-3">
					<label>{{ AdminLanguage::transAdmin('Naselje') }}</label>
					<select class="form-control" name="mesto" id="JSMesto">
						@foreach(AdminNarudzbine::mesta(Input::old('opstina') ? Input::old('opstina') : AdminNarudzbine::opstina_id($narudzbina_stavka->ulica_id)) as $key => $mesto)
						@if((Input::old('mesto') ? Input::old('mesto') : AdminNarudzbine::mesto_id($narudzbina_stavka->ulica_id)) == $mesto->narudzbina_mesto_id)
						<option value="{{ $mesto->narudzbina_mesto_id }}" selected>{{ $mesto->naziv }}</option>
						@else
						<option value="{{ $mesto->narudzbina_mesto_id }}">{{ $mesto->naziv }}</option>
						@endif
						@endforeach
					</select>
				</div> 

				<div class="columns medium-3">
					<label>{{ AdminLanguage::transAdmin('Ulica') }} ({{$narudzbina_stavka->ulica_id}})	 </label>
					<select class="form-control" name="ulica_id" id="JSUlica">
						@foreach(AdminNarudzbine::ulice(Input::old('mesto') ? Input::old('mesto') : AdminNarudzbine::mesto_id($narudzbina_stavka->ulica_id)) as $key => $ulica)
						@if((Input::old('ulica_id') ? Input::old('ulica_id') : $narudzbina_stavka->ulica_id) == $ulica->narudzbina_ulica_id)
						<option value="{{ $ulica->narudzbina_ulica_id }}" selected>{{ $ulica->naziv }}</option>
						@else
						<option value="{{ $ulica->narudzbina_ulica_id }}">{{ $ulica->naziv }}</option>
						@endif
						@endforeach
					</select>
				</div>

				<div class="columns medium-3"> 
					<label>{{ AdminLanguage::transAdmin('Broj') }}</label>
					<input name="broj" type="text" value="{{ htmlentities(Input::old('broj') ? Input::old('broj') : $narudzbina_stavka->broj) }}">
					<div class="error red-dot-error">{{ $errors->first('broj') ? $errors->first('broj') : "" }}</div>
				</div>

				<div class="columns medium-3">
					<label>{{ AdminLanguage::transAdmin('Način plaćanja') }}</label>
					<select class="form-control" name="web_nacin_placanja_id">
						{{ AdminPartneri::nacinPlacanja(!is_null(Input::old('web_nacin_placanja_id')) ? Input::old('web_nacin_placanja_id') : $narudzbina_stavka->web_nacin_placanja_id) }}
					</select>
				</div>

				<div class="columns medium-3"> 
					<label>{{ AdminLanguage::transAdmin('Transport plaća') }}</label>
					<select class="form-control" name="transport_placa">
						<option value="0" {{(Input::old('transport_placa') ? Input::old('transport_placa') : $narudzbina_stavka->transport_placa) == 0 ? 'selected' : '' }}>{{ AdminLanguage::transAdmin('Nalogodavac') }}</option>
						<option value="1" {{(Input::old('transport_placa') ? Input::old('transport_placa') : $narudzbina_stavka->transport_placa) == 1 ? 'selected' : '' }}>{{ AdminLanguage::transAdmin('Pošiljalac') }}</option>
						<option value="2" {{(Input::old('transport_placa') ? Input::old('transport_placa') : $narudzbina_stavka->transport_placa) == 2 ? 'selected' : '' }}>{{ AdminLanguage::transAdmin('Primalac') }}</option>
					</select>
				</div>

				<div class="columns medium-3"> 
					<label>{{ AdminLanguage::transAdmin('Kome se uplaćuje otkupnina') }}</label>
					<select class="form-control" name="otkup_prima">
						<option value="-1" {{(Input::old('otkup_prima') ? Input::old('otkup_prima') : $narudzbina_stavka->otkup_prima) == -1 ? 'selected' : '' }}>{{ AdminLanguage::transAdmin('Nema otkupa') }}</option>
						<option value="0" {{(!is_null(Input::old('otkup_prima')) ? Input::old('otkup_prima') : $narudzbina_stavka->otkup_prima) == 0 ? 'selected' : '' }}>{{ AdminLanguage::transAdmin('Nalogodavac') }}</option>
						<option value="1" {{(Input::old('otkup_prima') ? Input::old('otkup_prima') : $narudzbina_stavka->otkup_prima) == 1 ? 'selected' : '' }}>{{ AdminLanguage::transAdmin('Pošiljalac') }}</option>
					</select>
				</div>

				<div class="columns medium-3"> 
					<label>{{ AdminLanguage::transAdmin('Račun otkupa') }}</label>
					<input name="racun_otkupa" type="text" value="{{ htmlentities(Input::old('racun_otkupa') ? Input::old('racun_otkupa') : $narudzbina_stavka->racun_otkupa) }}">
					<div class="error red-dot-error">{{ $errors->first('racun_otkupa') ? $errors->first('racun_otkupa') : "" }}</div>
				</div>

				<div class="columns medium-3"> 
					<label>{{ AdminLanguage::transAdmin('Cena otkupa (u parama)') }}</label>
					<input name="cena_otkupa" type="text" value="{{ htmlentities(Input::old('cena_otkupa') ? intval(Input::old('cena_otkupa')) : intval($narudzbina_stavka->cena_otkupa)) }}">
					<div class="error red-dot-error">{{ $errors->first('cena_otkupa') ? $errors->first('cena_otkupa') : "" }}</div>
				</div>

				<div class="columns medium-3"> 
					<label>{{ AdminLanguage::transAdmin('Povratna dokumentacija') }}</label>
					<select class="form-control" name="odgovor">
						<option value="0" {{(!is_null(Input::old('odgovor')) ? Input::old('odgovor') : $narudzbina_stavka->odgovor) == 0 ? 'selected' : '' }}>{{ AdminLanguage::transAdmin('Ništa') }}</option>
						<option value="1" {{(Input::old('odgovor') ? Input::old('odgovor') : $narudzbina_stavka->odgovor) == 1 ? 'selected' : '' }}>{{ AdminLanguage::transAdmin('Otpremnica') }}</option>
						<option value="2" {{(Input::old('odgovor') ? Input::old('odgovor') : $narudzbina_stavka->odgovor) == 2 ? 'selected' : '' }}>{{ AdminLanguage::transAdmin('Plaćeni odgovor') }}</option>
						<option value="3" {{(Input::old('odgovor') ? Input::old('odgovor') : $narudzbina_stavka->odgovor) == 3 ? 'selected' : '' }}>{{ AdminLanguage::transAdmin('Povratnica lično') }}</option>
					</select>
				</div>

				<div class="columns medium-3"> 
					<label>{{ AdminLanguage::transAdmin('Napomena') }}</label>
					<input name="napomena" type="text" value="{{ (Input::old('napomena') != null) ? Input::old('napomena') : $narudzbina_stavka->napomena }}">
					<div class="error red-dot-error">{{ $errors->first('napomena') ? $errors->first('napomena') : "" }}</div>
				</div>

				<div class="columns medium-3"> 
					<label>{{ AdminLanguage::transAdmin('Opis') }}</label>
					<input name="opis" type="text" value="{{ (Input::old('opis') != null) ? Input::old('opis') : $narudzbina_stavka->opis }}">
					<div class="error red-dot-error">{{ $errors->first('opis') ? $errors->first('opis') : "" }}</div>
				</div>

				<div class="columns">&nbsp;</div>
			</div>
		</div>

		<div class="flat-box"> 
			
			<label>{{ AdminLanguage::transAdmin('Za svaku stavku') }}</label>
			
			<div class="row"> 
				<div class="columns medium-3"> 
					{{ AdminLanguage::transAdmin('Deo paketa') }}:
					<input name="broj_paketa" type="text" value="{{ htmlentities(Input::old('broj_paketa') ? Input::old('broj_paketa') : $narudzbina_stavka->broj_paketa) }}">
					<div class="error red-dot-error">{{ $errors->first('broj_paketa') ? $errors->first('broj_paketa') : "" }}</div>
				</div>

				<div class="columns medium-3"> 
					{{ AdminLanguage::transAdmin('Vrednost (u parama)') }}
					<input name="vrednost" type="text" value="{{ htmlentities(Input::old('vrednost') ? intval(Input::old('vrednost')) : intval($narudzbina_stavka->vrednost)) }}">
					<div class="error red-dot-error">{{ $errors->first('vrednost') ? $errors->first('vrednost') : "" }}</div>
				</div>

				<div class="columns medium-3"> 
					{{ AdminLanguage::transAdmin('Masa (u gramima)') }}
					<input name="masa" type="text" value="{{ htmlentities(Input::old('masa') ? intval(Input::old('masa')) : intval($narudzbina_stavka->masa)) }}">
					<div class="error red-dot-error">{{ $errors->first('masa') ? $errors->first('masa') : "" }}</div>
				</div>
			</div>

			<div class="btn-container center">
				<button type="submit" class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
			</div>
		</div>
	</form> 
</section>
