<div id="main-content" class="">
@include('admin/partials/product-tabs')

	<div class="row">
		<div class="column large-6 large-centered">
			<div class="flat-box">
				<form method="POST" action="{{AdminOptions::base_url()}}admin/opis_edit">

					<div class="row">
						<h3 class="title-med">{{ AdminLanguage::transAdmin('Opis artikla') }}</h3>
						<div class="column large-12 medium-12">
							<input type="hidden" id="roba_id" name="roba_id" value="{{ $roba_id ? $roba_id : 0 }}">
							<textarea @if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE'))) class="special-textareas" @endif id="mc" name="web_opis" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>{{ $web_opis }}</textarea>
						</div>
					</div> <!-- end of .row -->
					<br>
					<div class="row">
						<h3 class="title-med">{{ AdminLanguage::transAdmin('Fit') }}</h3>
						<div class="column large-12 medium-12">
							<textarea @if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE'))) class="special-textareas" @endif id="mc" name="opis" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>{{ $opis }}</textarea>
						</div>
					</div>
					<br>
					<div class="row">
						<h3 class="title-med">{{ AdminLanguage::transAdmin('Materijal') }}</h3>
						<div class="column large-12 medium-12">
							<textarea @if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE'))) class="special-textareas" @endif id="mc" name="napomena" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>{{ $napomena }}</textarea>
						</div>
					</div>
					<br>
					<div class="row">
						<h3 class="title-med">{{ AdminLanguage::transAdmin('Dobavljač') }}</h3>
						<div class="column large-12 medium-12">
							<textarea @if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE'))) class="special-textareas" @endif id="mc" name="akcija_blok" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>{{ $akcija_blok }}</textarea>
						</div>
					</div>
					<br>
					<div class="row">
						<h3 class="title-med">{{ AdminLanguage::transAdmin('Dostava') }}</h3>
						<div class="column large-12 medium-12">
							<textarea @if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE'))) class="special-textareas" @endif id="mc" name="naziv_racun_18" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>{{ $naziv_racun_18 }}</textarea>
						</div>
					</div>
					
					@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
						<div class="row">
							<div class="column large-12 medium-12">
								<input type="hidden" name="roba_id" value="{{ $roba_id ? $roba_id : 0 }}">
								<div class="btn-container center">
									<a class="setting-button btn btn-primary" href="#" id="JSsablon">{{ AdminLanguage::transAdmin('Unesi šablon') }}</a>
									<button type="submit" class="setting-button btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
								</div>
							</div>	
						</div> <!-- end of .row -->
					@endif
				</form>	
				 
			</div>

		</div>
	</div>
</div>
