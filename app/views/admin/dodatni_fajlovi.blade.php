<div id="main-content">
	
	@if(Admin_model::check_admin(array('ARTIKLI')) && Admin_model::check_admin(array('ARTIKLI_PREGLED')))
	@include('admin/partials/product-tabs')
	@endif

	@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
	<div class="row">
		<div class="medium-6 columns medium-centered">
			
			<form method="POST" action="{{AdminOptions::base_url()}}admin/dodatni_fajlovi_upload" enctype="multipart/form-data">
				<div class="flat-box">
					<h3 class="title-med">{{ AdminLanguage::transAdmin('Dodaj novi fajl') }}</h3>
					<input type="hidden" name="roba_id" value="{{ $roba_id ? $roba_id : 0 }}">
					
					@if(Session::has('mess'))
					<div class="row"> 
						<div class="columns error">
							{{ AdminLanguage::transAdmin('Fajl ne sme biti veci od 1MB') }}!
						</div>
					</div>
					@endif 

					<div class="row"> 
						<div class="column medium-6 field-group {{ $errors->first('naziv') ? 'error' : '' }}">
							<label>{{ AdminLanguage::transAdmin('Naziv') }}</label>
							<input type="text" name="naziv" value="{{ Input::old('naziv') != null ?  Input::old('naziv') : null }}">
						</div>

						<div class="column medium-3 field-group">
							<label>{{ AdminLanguage::transAdmin('Link') }}</label>
							<select name="check_link" id="check_link">
								{{ AdminSupport::selectCheck(Input::old('check_link') != 0 ?  1 : 0) }}
							</select>
						</div>

						<div class="column medium-3 field-group">
							<label>{{ AdminLanguage::transAdmin('Vrsta fajla') }}</label>
							<select name="vrsta_fajla">
								@foreach(DB::table('vrsta_fajla')->orderBy('vrsta_fajla_id','asc')->get() as $row)
								@if($row->vrsta_fajla_id == Input::old('vrsta_fajla'))
								<option value="{{ $row->vrsta_fajla_id }}" selected>{{ $row->ekstenzija }}</option>
								@else
								<option value="{{ $row->vrsta_fajla_id }}">{{ $row->ekstenzija }}</option>
								@endif
								@endforeach
							</select>
						</div>
					</div>

					<div class="row"> 
						<div class="columns"> 
							<div id="filePutanja" class="field-group {{ $errors->first('putanja_etaz') ? 'error' : '' }}" {{ Input::old('check_link') == 0 || Input::old('check_link') == null ? 'hidden' : '' }}>
								<label>{{ AdminLanguage::transAdmin('Putanja') }}</label>
								<input type="text" name="putanja" value="{{ Input::old('putanja') != null ?  Input::old('putanja') : null }}">
							</div>

							<div id="fileFile" class="field-group {{ $errors->first('file') ? 'error' : '' }}" {{ Input::old('check_link') == 0 || Input::old('check_link') == null ? '' : 'hidden' }}>
								<div class="bg-image-file"> 
									<input type="file" name="upl_file"> 
								</div>
							</div>
						</div>
					</div>
	  
					<div class="btn-container text-center"> 
						<button type="submit" class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
					</div> 
				</div>
			</form>

		</div>
	</div> 
	@endif

	@if(count($vrste_fajlova) > 0)
	<div class="row">
		<div class="medium-6 columns medium-centered">
			<div class="filter-files">
				@foreach($vrste_fajlova as $row)
				<input type="checkbox" class="JSCheckFilter" data-action='{"roba_id":"{{ $roba_id }}", "vrsta_id":"{{ $row->vrsta_fajla_id }}"}' {{ $extension_id == $row->vrsta_fajla_id ? 'checked' : '' }}>{{ $row->ext }}
				@endforeach
			</div>
		</div>
	</div>
	@endif

	<div class="row"> 
		<div class="medium-6 columns medium-centered">
			@foreach($fajlovi as $row)
			<div class="flat-box files-list">
				{{ AdminLanguage::transAdmin('Naziv') }}: {{ $row->naziv }} | {{ AdminLanguage::transAdmin('Vrsta') }}: {{ AdminSupport::getExtension($row->vrsta_fajla_id) }}
				<span class="files-list-items right">
					@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
					<a href="{{ AdminOptions::base_url().'admin/dodatni_fajlovi_delete/'.$roba_id.'/'.$row->web_file_id }}">{{ AdminLanguage::transAdmin('Obriši') }}</a> | 
					@endif
					<a href="{{ $row->putanja != null ? AdminOptions::base_url().$row->putanja : $row->putanja_etaz }}" target="_blank">{{ AdminLanguage::transAdmin('Vidi') }}</a>
				</span>
			</div>
			@endforeach
		</div>
	</div>

</div>