<!DOCTYPE html>
<html><head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
<title>PDF</title>
<style>
* { 
	box-sizing: border-box;
 	padding: 0;
	margin: 0; 
	line-height: 1;	
} 
body {
	font-size: 14px;  
	font-family: DejaVu Sans;
}
ul { list-style-type: none; }

table {border-collapse: collapse; margin-bottom: 15px; width: 100%;}

td, th {border: 1px solid #cacaca; text-align: left; padding: 3px;}
 
.row::after {content: ""; clear: both; display: table;}
[class*="col-"] { float: left; padding: 5px; display: inline-block;}

.col-1 {width: 8.33%;}
.col-2 {width: 16.66%;}
.col-3 {width: 25%;}
.col-4 {width: 33.33%;}
.col-5 {width: 41.66%;}
.col-6 {width: 50%;}
.col-7 {width: 58.33%;}
.col-8 {width: 66.66%;}
.col-9 {width: 75%;}
.col-10 {width: 83.33%;}
.col-11 {width: 91.66%;}
.col-12 {width: 100%;}
.inline-block { display: inline-block; }
.text-right{ text-align: right; }
.text-center{ text-align: center; } 
.relative { position: relative; }

img{
	max-width: 100%;
}
.header{
	border-bottom: 2px solid #cc0000;
	background-color: #f2f2f2;
	padding: 10px 5px;
}
.header .logo {
	max-height: 100px;  
	margin: 20px;
} 
.main-content {
	padding: 15px;   
}
.main-content .product-img{
	max-height: 220px;
} 
.main-content .foo_1{
	font-size: 20px;
	margin: 15px 0;
	padding: 5px 0;
	border-bottom: 1px solid #ddd;
} 
.main-content .foo_2{
	font-size: 25px;
	margin: 15px 0;
	color: #cc0000;
} 
.main-content .foo_2 span{
	font-size: 90%;
} 
.main-content .foo_3 .ul_1,.main-content .foo_3 .genkar{ 
	border: 1px solid #ddd; 
}  
.main-content .foo_3 .ul_1 .li_1{ 
	width: 30%; 
}
.main-content .foo_3 .ul_1 .li_2{ 
	width: 60%; 
	border-bottom: 1px solid #ddd;
} 
.main-content .foo_3 ul li{
    font-size: 12px; 
    padding: 2px 10px;      
    width: 45%;
    display: inline-block;   
}      
.main-content .foo_3 .genkar li{
  	border-bottom: 1px solid #ddd;
}    
.footer{
	background: #464853; 
	color: #fff;
	padding: 20px 15px;
	position: absolute;
	bottom: 0;
	font-size: 12px;
}
</style>
</head><body>
	<div class="container relative"> 
		<div class="row header">
			<div class="col-6">
				<img class="logo" src="{{ Options::domain()}}{{Options::company_logo()}}" alt="logo">
			</div>
			<div class="col-5">
				<div>
					<p>Firma: {{Options::company_name()}}</p>
					<p>Adresa: {{Options::company_adress()}}</p>
					<p>Telefon: {{Options::company_phone()}}</p>
					<p>Fax: {{Options::company_fax()}}</p>
					<p>PIB: {{Options::company_pib()}}</p>
					<p>E-mail: {{Options::company_email()}}</p>
					<p>Web: {{Options::base_url()}}</p>
				</div>
			</div>
		</div>
		@if(!is_null($katalog->vazi_od))
		<div class="row">
			Važi od: {{ date("d.m.Y", strtotime($katalog->vazi_od)) }}. god
		</div>
		@endif

		<ul>	
    		@foreach(Catalog::types() as $type)
    			@if(count($typeArticles = Catalog::typeArtcles($katalog->katalog_id,$type->tip_artikla_id)) > 0)
				<li>{{ $type->naziv }}</li>
				<li>
				<table class="">
					<tr>
						@foreach($katalog_polja as $katalog_polje)
							<td class="cell">{{$katalog_polje->title}}</td>
						@endforeach
			        </tr>
		        	@foreach($typeArticles as $article)
		        	<tr>
						@foreach($katalog_polja as $katalog_polje)
							@if($katalog_polje->naziv == 'web_cena')
							<td class="cell">{{ (Cart::cena(Product::get_price($article->roba_id)/1.2) )}}</td>

							@elseif($katalog_polje->naziv == 'web_cena_pdv')
							<td class="cell">{{ Cart::cena(Product::get_price($article->roba_id)) }}</td>

							@elseif($katalog_polje->naziv == 'slika')
							<td class="cell"><img src="{{ Options::domain() }}{{ Product::web_slika($article->roba_id) }}"/></td>

							@elseif($katalog_polje->naziv == 'naziv')
							<td class="cell">{{ Product::seo_title($article->roba_id) }}</td>

							@elseif($katalog_polje->naziv == 'opis')
							<td class="cell">{{ $article->web_opis }}</td>

							@elseif($katalog_polje->naziv == 'karakteristike')
							<td class="cell">{{ Product::get_karakteristike_table($article->roba_id) }}</td>

							@endif
						@endforeach
		        	</tr>
		            @endforeach
		        </table>
		        </li>
		        @endif
            @endforeach
	    </ul>
	 			
	</div>  
</body></html>
