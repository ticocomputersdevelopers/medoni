@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page') 
<div class="container">
 	
  
  	<div class="text-center"> 
		
 		<br>
		<h2><span class="section-title">{{ Language::trans('Greška') }}</span></h2> 
    	<h4>{{ Language::trans('Žao nam je, stranica nije dostupna') }}.</h4>
    
    </div>
		 
</div> 
@endsection