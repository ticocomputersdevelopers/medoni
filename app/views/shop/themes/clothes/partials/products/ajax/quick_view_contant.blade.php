<script src="{{Options::domain()}}js/slick.min.js"></script>


<div class="d-content JSmain relative"> 
    <div class="container"> 

        <div id="product-preview" class="clearfix relative">

            <div class="row flex"> 
            
                <div class="JSproduct-preview-image col-md-7 col-sm-7 col-xs-12 no-padding">
                    <div class="row"> 

                        <div class="col-md-10 col-sm-10 col-xs-12 disableZoomer no-padding product-preview-main-img-wrap relative">  
                            <!-- <img class="JSmain_img img-responsive" src="{{ Options::domain() }}{{ $slika_big }}" alt="{{ Product::seo_title($roba_id)}}" />   -->
                            <div class="JSproduct-main-slick row">
                                @foreach($slike as $image)
                                <div class="col-md-12 col-sm-12 col-xs-12 JSproducts-main-img no-padding">
                                    <img alt="{{ Options::domain().$image->putanja }}" id="{{ Options::domain().$image->web_slika_id }}" src="{{ Options::domain().$image->putanja }}"/>
                                </div>
                                @endforeach
                                @if(!Cart::exist_in_wish_list($roba_id,All::getClientId()))
                                <button type="button" class="article-like-it JSadd-to-wish" data-roba_id="{{$roba_id}}" title="{{ Language::trans('') }}"><i class="sprite sprite-small-wish"></i></button> 
                                @else
                                <button type="button" class="article-like-it JSukloni" data-roba_id="{{$roba_id}}" title="{{ Language::trans('') }}"><i class="sprite sprite-small-wish-full"></i></button> 
                                @endif
                                <button type="button" class="article-full-it"><i class="sprite sprite-fullscreen"></i></button> 
                            </div>

                            <div class="full-it-modal">
                                @foreach($slike as $image)
                                    <div class="full-it-img JSproducts-main-img">
                                        <img class="img-responsive" src="{{ Options::domain().$image->putanja }}" target="_blank">
                                    </div>          
                                @endforeach
                                <i class="sprite sprite-close"></i>
                            </div>

                            <!-- STICKER -->
                            <div class="product-sticker flex">
                                @if( B2bArticle::stiker_levo($roba_id) != null )
                                    <a class="article-sticker-img">
                                        <img class="img-responsive" src="{{ Options::domain() }}{{B2bArticle::stiker_levo($roba_id) }}"  />
                                    </a>
                                @endif 
                                
                                @if( B2bArticle::stiker_desno($roba_id) != null )

                                       <a class="article-sticker-img clearfix">
                                        <img class="img-responsive pull-right" src="{{ Options::domain() }}{{B2bArticle::stiker_desno($roba_id) }}"  />
                                    </a>
                                @endif   
                            </div>

                            <!-- CUSTOM MODAL -->
                            <div class="JSmodal">
                                <div class="flex full-screen relative"> 
                                  
                                    <div class="modal-cont relative text-center"> 
                                  
                                        <div class="inline-block relative"> 
                                            <span class="JSclose-modal"><i class="fas fa-times"></i></span>
                                      
                                            <img src="" class="img-responsive" id="JSmodal_img" alt="modal image">
                                        </div>

                                        <button class="btn JSleft_btn"><i class="fas fa-chevron-left"></i></button>
                                        <button class="btn JSright_btn"><i class="fas fa-chevron-right"></i></button>
                                    </div>
                            
                                </div>
                            </div>

                            <div class="product-preview-brands">
                                @if($proizvodjac_id != -1)
                                    @if( Product::slikabrenda($roba_id) != null )
                                    <a class="article-brand-img inline-block valign" href="{{Options::base_url()}}{{Url_mod::slug_trans('proizvodjac')}}/{{Url_mod::slug_trans(Product::get_proizvodjac($roba_id)) }}">
                                        <img src="{{ Options::domain() }}{{product::slikabrenda($roba_id) }}" alt="{{ product::get_proizvodjac($roba_id) }}" />
                                    </a>
                                    @else
                                    <a class="artical-brand-text inline-block" href="{{Options::base_url()}}{{Url_mod::slug_trans('proizvodjac')}}/{{Url_mod::slug_trans(Product::get_proizvodjac($roba_id)) }}">{{ product::get_proizvodjac($roba_id) }}</a>
                                    @endif                                   
                                @endif
                            </div>

                        </div> 

                        <div class="col-md-2 col-sm-2 col-xs-12 JSproduct-gallery flex">  
                            <!-- <div class="JSproduct-gallery-slick"> -->
                                @foreach($slike as $image)
                                <div class="product-gallery-img img-gallery">
                                    <img alt="{{ Options::domain().$image->putanja }}" id="{{ Options::domain().$image->web_slika_id }}" src="{{ Options::domain().$image->putanja }}"/>
                                </div>
                                @endforeach
                            <!-- </div> -->
                        </div> 

                    </div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 additional_img">    
                            @foreach($glavne_slike as $slika)
                            <a class="inline-block text-center" href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($roba_id))}}/{{$slika->web_slika_id}}"> 
                                <img src="{{ Options::domain().$slika->putanja }}" alt=" {{ Options::domain().$slika->putanja }}" class="img-responsive inline-block"> 
                            </a>
                            @endforeach
                        </div>
                    </div>
                </div>


                <div class="product-preview-info col-md-4 col-sm-4 col-xs-12 flex no-padding">

                   <!-- ADMIN BUTTON-->
                    @if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array('ARTIKLI')) AND Admin_model::check_admin(array('ARTIKLI_PREGLED')))
                        <div class="admin-article inline-block"> 
                            @if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
                                <a class="article-level-edit-btn JSFAProductModalCall" data-roba_id="{{$roba_id}}" href="javascript:void(0)" rel="nofollow">{{ Language::trans('IZMENI ARTIKAL') }}</a> 
                            @endif
                            <span class="supplier"> {{ Product::get_dobavljac($roba_id) }}</span> 
                            <span class="supplier">{{ Language::trans('NCP') }}: {{ Product::get_ncena($roba_id) }}</span>
                        </div>
                    @endif


                    <h1 class="article-heading">{{ Product::seo_title($roba_id) }}</h1>
                    
                    <!-- PRICE -->
                    <div class="product-preview-price">
                    @if(Product::getStatusArticlePrice($roba_id) == 1)
                        @if(Product::pakovanje($roba_id))
                            <div>
                                <span class="price-label">{{Language::trans('Pakovanje')}}: </span>
                                <span class="price-num">{{ Product::ambalaza($roba_id) }}</span>
                            </div>
                        @endif     

                        @if(All::provera_akcija($roba_id))                                      
                           
                            @if(Product::get_mpcena($roba_id) != 0)
                            <div>
                                <span class="price-label">{{Language::trans('MP cena')}}: </span>
                                <span class="price-num">{{ Cart::cena(Product::get_mpcena($roba_id)) }}</span>
                            </div>
                            @endif 

                            <div>
                                <span class="price-label" >{{Language::trans('Akcija')}}:</span>
                                <span class="JSaction_price price-num" data-akc_cena="{{Product::get_price($roba_id)}}">{{ Cart::cena(Product::get_price($roba_id,true,false,(!is_null(Input::old('kamata')) ? Input::old('kamata') : 0 ))) }}</span>
                            </div>

                            @if(Product::getPopust_akc($roba_id)>0)
                             <div>
                                <span class="price-label">{{Language::trans('Ušteda')}}:</span>
                                <span class="price-num">{{ Cart::cena(Product::getPopust_akc($roba_id)) }}</span> 
                            </div>
                            @endif

                        @else

                            @if(Product::get_mpcena($roba_id) != 0)
                                <div>
                                    <span class="price-label">{{Language::trans('MP cena')}}: </span>
                                    <span class="price-num">{{ Cart::cena(Product::get_mpcena($roba_id)) }}</span>
                                </div>
                            @endif

                            <div>
                                <span class="price-label">{{Language::trans('Web cena')}}:</span>
                                <span class="JSweb_price price-num" data-cena="{{Product::get_price($roba_id)}}">
                                   {{ Cart::cena(Product::get_price($roba_id,true,false,(!is_null(Input::old('kamata')) ? Input::old('kamata') : 0 ))) }}
                                </span>
                            </div>

                            @if(Product::getPopust($roba_id)>0)
                                @if(AdminOptions::web_options(132)==1)
                                    <span class="price-label">{{Language::trans('Ušteda')}}:</span>
                                    <span class="price-num">{{ Cart::cena(Product::getPopust($roba_id)) }}</span>
                                @endif
                            @endif

                        @endif
                    @endif 
                    </div>

                    <div class="rate-me-artical hidden">
                        {{ Product::getRating($roba_id) }}
                    </div>

                    <div class="add-to-cart-area clearfix">     
                         @if(Product::getStatusArticle($roba_id) == 1)
                            @if(Cart::check_avaliable($roba_id) > 0)
                            <form method="POST" action="{{ Options::base_url() }}product-cart-add" id="JSAddCartForm" class="article-form"> 

                            @if(Product::check_osobine($roba_id))
                                
                                @foreach(Product::osobine_nazivi($roba_id) as $osobina_naziv_id)
                                    <div class="attributes">
                                       
                                        <div class="attr-btn"><span class="JSattr-btn-name">{{ Product::find_osobina_naziv($osobina_naziv_id,'naziv') }}</span><i class="sprite sprite-down"></i></div>

                                        <ul class="attr-drop-menu">
                                            @foreach(Product::osobine_vrednosti($roba_id,$osobina_naziv_id) as $osobina_vrednost_id)
                                            <label class="relative attr">
                                            <!-- <label class="relative attr" style="background-color: {{ Product::find_osobina_vrednost($osobina_vrednost_id, 'boja_css') }}"> -->

                                                <input type="radio" name="osobine{{ $osobina_naziv_id }}" value="{{ $osobina_vrednost_id }}" {{ Product::check_osobina_vrednost($roba_id,$osobina_naziv_id,$osobina_vrednost_id,Input::old('osobine'.$osobina_naziv_id)) }}>
                                            
                                                <!-- <span class="inline-block">{{ Product::osobina_vrednost_checked($osobina_naziv_id, $osobina_vrednost_id) }}</span> -->
                                                <span class="inline-block">{{ Product::find_osobina_vrednost($osobina_vrednost_id, 'vrednost') }}</span>
     
                                            </label>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endforeach

                            @endif

                            @if(AdminOptions::web_options(313)==1) 
                            <div class="num-rates"> 
                                <div> 
                                    <div class="inline-block lorem-1">{{ Language::trans('Broj rata') }}</div>
                                </div>
                                <select class="JSKamata" name="kamata">
                                    {{ Product::broj_rata(Input::old('kamata')) }}
                                </select>
                            </div>
                            @endif
                            
                            <div class="printer inline-block hidden" title="{{ Language::trans('Štampaj') }}">  
                                <a href="{{Options::base_url()}}stampanje/{{ $roba_id }}" target="_blank" rel="nofollow"><i class="fas fa-print"></i></a>
                            </div>

                            @if(Cart::kupac_id() > 0)
                            <button type="button" class="like-it JSadd-to-wish hidden" data-roba_id="{{$roba_id}}" title="{{ Language::trans('Dodaj na listu želja') }}"><i class="far fa-heart"></i></button> 
                            @else
                            <button type="button" class="like-it JSnot_logged hidden" data-roba_id="{{$roba_id}}" title="{{ Language::trans('Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima') }}"><i class="far fa-heart"></i></button> 
                            @endif  

                            <input type="hidden" name="roba_id" value="{{ $roba_id }}">
                            <div class="inline-block hidden">&nbsp;{{Language::trans('Količina')}}&nbsp;</div>
                            <input type="text" name="kolicina" class="cart-amount hidden" value="{{ Input::old('kolicina') ? Input::old('kolicina') : '1' }}">

                            <!-- ADD TO CART BTNS -->

                            <button class="JSadd-to-cart article-button">{{ Language::trans('U korpu') }} <i class="sprite sprite-cart-small"></i></button>

                            <!-- <button type="submit" id="JSAddCartSubmit" class="article-button">{{Language::trans('U korpu')}} <i class="sprite sprite-cart-small"></i></button> -->

                            <a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($roba_id))}}" class="flex link-to-article-btn"><span>{{Language::trans('Vidi artikl')}}</span> <i class="sprite sprite-shout"></i></a>

                            <input type="hidden" name="projectId" value=""> 
                            
                            <div class="hidden">{{ $errors->first('kolicina') ? $errors->first('kolicina') : '' }}</div>  
                         </form>
                        @else
                            <button class="button not-available article-button">{{Language::trans('Nije dostupno')}}</button>            
                        @endif

                        @else
                        <button class="button article-button" data-roba-id="{{$roba_id}}">
                            {{ Product::find_flag_cene(Product::getStatusArticle($roba_id),'naziv') }}
                        </button>
                        @endif
                    </div> 

                    @if(Options::web_options(120))
                        @if(count($fajlovi) > 0)
                            <div class="size-guide pointer">
                                <span>{{Language::trans('Veličine')}}</span>
                            </div>

                            <div class="size-guide-modal">
                                    <div>
                                        @foreach($fajlovi as $row)
                                            @if($row->vrsta_fajla_id == 6)
                                                <div class="files-list-img">
                                                    <img class="img-responsive" src="{{ $row->putanja != null ? Options::domain().$row->putanja : $row->putanja_etaz }}" target="_blank">
                                                </div>                                
                                                <?php break; ?>
                                            @endif
                                        @endforeach
                                    </div>
                                <i class="sprite sprite-close"></i>
                            </div>
                        @endif
                    @endif
                    
                </div>

                <span class="product-preview-close col-md-1 col-sm-1 col-xs-1 hidden-xs">
                    <i class="sprite sprite-close"></i>
                </span>
            </div>
        </div>
    </div>
</div>


<script>
$(document).ready(function () {
    if ($('.JSproduct-main-slick').length) { 
        $('.JSproduct-main-slick').slick({ 
            autoplay: false,
            slidesToShow: 1,
            draggable: false,
            asNavFor: $('.JSproduct-gallery-slick'),
            slidesToScroll: 1
        }); 
    }

    $('.product-preview-close i').on('click', function() {
        $('.product-card-details').hide();
    });

    $('.attr-btn').on('click', function() {
        $('.attr-drop-menu').toggle();
        $(this).toggleClass('attr-btn-opened');
        $(this).find('i').toggleClass('sprite-up');
    });

    $(document).on('click', function(e){
        var target = $(e.target)
;        if(!target.is($('.attributes, .attributes *'))) { 
            $('.attr-drop-menu').hide();
            $('.attr-btn').find('i').removeClass('sprite-up');
            $('.attr-btn').removeClass('attr-btn-opened');
        } 
    }); 

    $('.size-guide').on('click', function() {
        $('.size-guide-modal').show();
        $('body').addClass('size-modal-after');
    });

    $(document).on('click', function(e){
        var target = $(e.target);
        if(!target.is($('.size-guide-modal, .size-guide-modal img, .size-guide, .size-guide *'))) { 
            $('.size-guide-modal').hide();
            $('body').removeClass('size-modal-after');
        } 
    });
    
    $('.article-full-it').on('click', function() {
        var currentImgLink = $('.JSproducts-main-img.slick-active img').attr('src');
        $('.full-it-modal').show();
        $('body').addClass('full-it-preview');
        $('.full-it-img img').attr('src', currentImgLink);
    });

    $(document).on('click', function(e){
        var target = $(e.target);
        if(!target.is($('.full-it-modal, .full-it-modal img, .article-full-it, .article-full-it *'))) { 
            $('.full-it-modal').hide();
            $('body').removeClass('full-it-preview');
        } 
    }); 

    $('.JSproduct-gallery').each(function(i) {
        $(this).find('.img-gallery').each(function(i) {
            $(this).attr('JSgallery-index', i);
        });
    });
    
    $('.JSproduct-gallery .img-gallery').on('click', function() {
        clickedGalleryIndex = $(this).attr('JSgallery-index');
        $('.JSproduct-main-slick').slickGoTo(clickedGalleryIndex);
    });

    $('.attr').on('click', function() {
        var attrName = $(this).find('span').text();
        $('.JSattr-btn-name').text(attrName);

        $('.attr-drop-menu').hide();
        $('.attr-btn').find('i').removeClass('sprite-up');
        $('.attr-btn').removeClass('attr-btn-opened');
    });
    
}); 


</script>