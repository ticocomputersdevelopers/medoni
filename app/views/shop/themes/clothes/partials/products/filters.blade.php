<?php // echo $kara; ?>
<div class="filters flex">
<!-- CHECKED FILTERS -->
   		<div>
		    <div class="clearfix JShidden-if-no-filters">
		    	<span>{{ Language::trans('Izabrani filteri') }}:</span>
		    	<a class="JSreset-filters-button inline-block pull-right" role="button" href="javascript:void(0)" rel=”nofollow”>{{ Language::trans('Poništi filtere') }} <span class="fas fa-times"></span></a>
		    </div> 
			
			<ul class="selected-filters">
				<?php $br=0;
				foreach($niz_proiz as $row){
					$br+=1;
					if($row>0){
				?>
					<li>
					    {{All::get_manofacture_name($row)}}     
					    <a href="javascript:void(0)" rel=”nofollow”>
					        <span class="JSfilter-close fas fa-times" data-type="1" data-rb="{{$br}}" data-element="{{$row}}"></span>
					    </a>
					</li>
				<?php }}

				$br=0;
				foreach(All::getGroupedCharac($niz_karakteristike) as $key => $val){
					$br+=1;
					if($val != 0){
				?>
					<li>
						{{Language::trans($key)}}
						<a href="javascript:void(0)" rel=”nofollow”>
					        <span class="JSfilter-close fas fa-times" data-type="2" data-rb="{{$br}}" data-element="{{$val}}"></span>
					    </a>
					</li>
					<?php }}?>
			</ul>
			<input type="hidden" id="JSmanufacturer_ids" value="{{$proizvodjac_ids}}"/>
			<input type="hidden" id="JSmanufacturer_attr" value="{{$kara}}"/>
		</div>	

	  
	<!-- END CHECKED -->
	  		<ul class="flex filter-box-wrap">

			    <!-- FILTER ITEM -->
			    @if(count($manufacturers)>0)
				<li class="filter-box">
                    <a href="javascript:void(0)" class="filter-links JSfilters-slide-toggle" rel=”nofollow”>{{ Language::trans('Proizvođač') }}
                    	<!-- <span class="choosed_filter">@foreach($manufacturers as $key => $value) @if(in_array($key,$niz_proiz)) {{ All::get_manofacture_name($key).' ' }} @endif @endforeach </span> -->
                   <i class="sprite sprite-down"></i></a>
				 	 	<div class="JSfilters-slide-toggle-content">
							@foreach($manufacturers as $key => $value)
							@if(in_array($key,$niz_proiz))
								<label>
									<input name="proizvodjac[]" onchange="filters_send()" value="{{ $key }}" type="checkbox" checked>
									<span class="filter-text">
										{{All::get_manofacture_name($key)}} 
									</span>
									<span class="filter-count">
										@if(Options::filters_type()) {{ $value }} @endif
									</span>
								</label>
							@else
								<label>
									<input name="proizvodjac[]" onchange="filters_send()" value="{{ $key }}" type="checkbox">
									<span class="filter-text">
										{{All::get_manofacture_name($key)}} 
									</span>
									<span class="filter-count">
										@if(Options::filters_type()) {{ $value }} @endif
									</span>
								</label>
							@endif
							@endforeach
						</div>				 				 
					</li>
				@endif

				@if($characteristics > 0)
					@foreach($characteristics as $keys => $values)
					<li class="filter-box">
		 				<a  href="javascript:void(0)" class="filter-links JSfilters-slide-toggle" rel=”nofollow”>
							{{Language::trans($keys)}} 
							<!-- <span class="choosed_filter"> @foreach($values as $key => $value) @if(in_array($key, $niz_karakteristike)) {{ All::get_fitures_name($key) }} @endif @endforeach </span> -->
							<i class="sprite sprite-down"></i>  							
						</a>
						<div class="JSfilters-slide-toggle-content">
							@foreach($values as $key => $value)
							<?php if(!array_diff(explode('-',$value['ids']),$niz_karakteristike)){ ?>
							<label>
								<input type="checkbox" onchange="filters_send()" name="vrednost[]" value="{{ $value['ids'] }}" checked>
								<span class="filter-text">
									{{ Language::trans($key) }}
								</span>
								<span class="filter-count">
									@if(Options::filters_type()) {{ $value['count'] }} @endif
								</span>
							</label>
							<?php }else {?>
							<label>
								<input type="checkbox" onchange="filters_send()" name="vrednost[]" value="{{ $value['ids'] }}"> 
								<span class="filter-text">
									{{ Language::trans($key) }}
								</span>
								<span class="filter-count">
									@if(Options::filters_type()) {{ $value['count'] }} @endif
								</span>
							</label>
							<?php } ?>
							@endforeach
						</div>
					</li>
					@endforeach	
				@endif
						
            </ul>	 

            @if(Options::product_currency()==1)
	            <div class="dropdown inline-block currency-list-in-filters">
	            	 <button class="btn dropdown-toggle filter-links" type="button" data-toggle="dropdown">
	            	 	 {{Articles::get_valuta()}} 
	            	 	 <!-- <span class="caret"></span> -->
	            	 	 <i class="sprite sprite-down"></i>
	            	 </button>
	                 
	                <ul class="dropdown-menu currency-list">
	                	@foreach(DB::table('valuta')->where('ukljuceno',1)->orderBy('izabran','desc')->get() as $valuta)
	                    <li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('valuta') }}/{{ $valuta->valuta_id }}" rel="nofollow">{{ Language::trans($valuta->valuta_sl) }}</a></li>
	                    @endforeach
	                </ul>
	            </div>
	        @endif
	        @if(Options::product_sort()==1)
	            <div class="dropdown inline-block currency-list-in-filters"> 
	            	 <button class="btn dropdown-toggle filter-links" type="button" data-toggle="dropdown">
		                 {{Articles::get_sort()}}
		                 <!-- <span class="caret"></span> -->
		                 <i class="sprite sprite-down"></i>
	            	</button>
	                <ul class="dropdown-menu currency-list">
	                	@if(Options::web_options(207) == 0)
	                    <li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sortiranje') }}/price_asc" rel="nofollow">{{ Language::trans('Cena min') }}</a></li>
	                    <li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sortiranje') }}/price_desc" rel="nofollow">{{ Language::trans('Cena max') }}</a></li>
	                    @else
	                    <li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sortiranje') }}/price_desc" rel="nofollow">{{ Language::trans('Cena max') }}</a></li>
	                    <li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sortiranje') }}/price_asc" rel="nofollow">{{ Language::trans('Cena min') }}</a></li>
	                    @endif
	                    <li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sortiranje') }}/news" rel="nofollow">{{ Language::trans('Najnovije') }}</a></li>
	                    <li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sortiranje') }}/name" rel="nofollow">{{ Language::trans('Prema nazivu') }}</a></li>
	                    <li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sortiranje') }}/rbr" rel="nofollow">{{ Language::trans('Popularni') }}</a></li>
	                </ul>
	            </div>
	        @endif

		<input type="hidden" id="JSfilters-url" value="{{$url}}" />
 
<!-- SLAJDER ZA CENU -->
	<div class="text-center hidden"> 
		<br>	
		<span id="JSamount" class="filter-price"></span>
		<div id="JSslider-range"></div><br>
	</div>
	
	<script>
		var max_web_cena_init = {{ $max_web_cena }};
		var min_web_cena_init = {{ $min_web_cena }};
		var max_web_cena = {{ $cene!='0-0' ? explode('-',$cene)[1] : $max_web_cena }};
		var min_web_cena = {{ $cene!='0-0' ? explode('-',$cene)[0] : $min_web_cena }};
		var kurs = {{ Session::has('valuta') ? (Session::get('valuta')!="2" ? 1 : Options::kurs()) : 1 }};
	</script>
</div>

 
 