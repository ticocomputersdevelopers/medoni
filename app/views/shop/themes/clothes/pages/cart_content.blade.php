@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
 
<div class="cart"> 

	@if(Session::has('b2c_korpa') and Cart::broj_cart() >= 1 )

	<!-- <h2><span class="page-title">{{ Language::trans('Korpa') }}</span></h2> -->

	<h2 class="cart-heading">{{ Language::trans('1. Izabrani artikli') }}</h2>

	@if(Session::has('failure-message'))
	<h4>{{ Session::get('failure-message') }}</h4>
	@endif
	<ul class="cart-labels row hidden hidden-sm hidden-xs">
		<li class="col-md-3 col-sm-3 col-sm-offset-2 col-md-offset-2">{{ Language::trans('Proizvod') }}</li>			 
		<li class="col-md-2 col-sm-2">{{ Language::trans('Cena') }}</li>
		<li class="col-md-2 col-sm-2">{{ Language::trans('Količina') }}</li>
		<li class="col-md-2 col-sm-2">{{ Language::trans('Ukupno') }}</li> 	 
	</ul>
	@foreach(DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',Cart::korpa_id())->orderBy('web_b2c_korpa_stavka_id','asc')->get() as $row)
	<div class="JScart-item flex relative">	 
		<div class="cart-item-image">
			<img src="{{ Options::domain() }}{{Product::web_slika($row->roba_id) }}" alt="{{ Product::short_title($row->roba_id) }}" class="cart-image img-responsive" />
		</div>

		<div class="cart-item-info flex">
			<span class="cart-name"> 
				<a class="inline-block" href="{{ Options::base_url() }}{{Url_mod::slug_trans('artikal')}}/{{ Url_mod::slugify(Product::seo_title($row->roba_id)) }}">
					{{ Product::short_title($row->roba_id) }}
				</a>
			</span>

			<span class="cart-name"> 
				<!-- MODIFIED -->
				{{Product::getOsobineStr($row->roba_id,$row->osobina_vrednost_ids)}}   
			</span>

			<div>
				<div class="cart-price hidden-xs"><span class="cart-price hidden-xs">{{ Cart::cena($row->jm_cena) }}</span></div>
				<div class="cart-total-price "><span class="JScart-item-price" data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}">{{ Cart::cena($row->jm_cena*$row->kolicina) }}</span></div>
			</div>
		</div>

		<div class="cart-item-add cart-add-amount clearfix flex">
			<a data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}" class="JScart-less" href="javascript:void(0)" rel=”nofollow”><i class="sprite sprite-cart-minus"></i></a>

			<input type="text" class="JScart-amount" value="{{ round($row->kolicina) }}" onkeypress="validate(event)" data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}">
			
			<a data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}" class="JScart-more" href="javascript:void(0)" rel=”nofollow”><i class="sprite sprite-cart-plus"></i></a>
		</div>

		<div class="cart-item-wish">
            <button type="button" class="JSmove_to_wish_list_from_cart JSdelete_cart_item" data-roba_id="{{$row->roba_id}}" data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}" title="{{ Language::trans('Prebaci artikal u listu želja') }}"><i class="sprite sprite-cart-heart"></i></button> 
		</div>

		<div class="cart-remove">
			<a href="javascript:void(0)" data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}" class="remove-item JSdelete_cart_item" rel=”nofollow”><span class="sprite sprite-cart-close"></span></a>
		</div>			 
	</div>
	@endforeach

	<button class="button" id="JSDeleteCart">{{ Language::trans('Isprazni korpu') }}</button>

	@if(Options::web_options(150) && !Options::web_options(133))
	<div class="cart-free-del relative">
	<div class="cart-free-del">
	@if(Product::check_free_delivery(Cart::cart_ukupno()))
	<span class="JSfree_delivery free-delivery-free"> {{ Language::trans('Besplatna dostava') }} </span> 

	<span class="JSfree_delivery free-delivery-price hidden"> <span class="hidden JSdeliverypriceflag" priceflag="{{ Product::check_free_delivery_additional(Cart::cart_ukupno()) }}"></span> {{ Language::trans('Povećajte svoj iznos za') }} <span class="JSdelivery-price-calc"></span>{{ Language::trans(',00 RSD. kako bi dobili besplatnu dostavu') }}</span> 

			
	@else
	<span class="JSfree_delivery free-delivery-price"> <span class="hidden JSdeliverypriceflag" priceflag="{{ Product::check_free_delivery_additional(Cart::cart_ukupno()) }}"></span>{{ Language::trans('Povećajte svoj iznos za') }} <span class="JSdelivery-price-calc"></span>{{ Language::trans(',00 RSD. kako bi dobili besplatnu dostavu') }}</span> 

	<span class="JSfree_delivery free-delivery-free hidden"> {{ Language::trans('Besplatna dostava') }} </span> 

							
	@endif
	</div>
		<i class="sprite sprite-free-del"></i> 
	</div>
	@endif

	@if(Options::web_options(318)==1)
	<div class="voucher flex">
		<input type="text" name="vaucer_code" value="{{ htmlentities(Input::old('vaucer_code') ? Input::old('vaucer_code') : '') }}" placeholder="{{ Language::trans('Vaučer kod') }}">
		<button class="hidden">{{ Language::trans('Dodaj') }}</button>
		<div class="error red-dot-error">{{ Session::has('vaucer_error') ? Session::get('vaucer_error') : '' }}</div>

		<div class="row hidden">
			<div class="col-md-12 col-sm-12 col-xs-12 form-group">
				<label>{{ Language::trans('Vaučeri') }}</label>
				<div>{{ Language::trans('Maksimalni popust preko vaučera koji možete iskoristiti u ovoj kupovini je') }} <span id="JSMaxUsingVoucherPrice">{{ Cart::cena(Cart::vauceriPopustCenaKorpa()) }}</span></div>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12 form-group">
				<label>{{ Language::trans('Unesite kod sa vašeg vaučera') }}:</label>
				<input type="text" name="vaucer_code" tabindex="6" value="{{ htmlentities(Input::old('vaucer_code') ? Input::old('vaucer_code') : '') }}">
			</div>
		</div>		
	</div>
	@endif

	<div class="cart-price-wrap">  
		<?php $troskovi = Cart::troskovi(); ?>
		
		<div id="JSExpensesContent" {{ !($troskovi>0 AND Input::old('web_nacin_isporuke_id') != 2) ? 'hidden="hidden"' : '' }}>		
			<div>
				<span class="sum-label">{{ Language::trans('Cena artikala') }}: </span>
				<span class="JSdelivery_total_amount sum-amount">{{Cart::cena(Cart::cart_ukupno())}}</span>
			</div>

			<div class="JSDelivery">
				<span class="sum-label">{{ Language::trans('Troškovi isporuke') }}: </span>
				<span class="JSexpenses sum-amount JSexpenses-for-append" data-troskovi="{{ $troskovi }}">{{ Cart::cena($troskovi) }}</span>
			</div>

			<div class="cart-final-price">
				<span class="sum-label">{{ Language::trans('Ukupno') }}: </span>
				<span class="JStotal_amount JStotal_amount_weight sum-amount ">{{Cart::cena(Cart::cart_ukupno()+$troskovi)}}</span>
			</div>
		</div>

		<div id="JSWithoutExpensesContent" {{ ($troskovi>0 AND Input::old('web_nacin_isporuke_id') != 2) ? 'hidden="hidden"' : '' }}>
			<div>
				<span class="sum-label text-right">{{ Language::trans('Cena artikala') }} </span>
				<span class="JSdelivery_total_amount sum-amount">{{Cart::cena(Cart::cart_ukupno())}}</span>
			</div>
			<div class="JSfree_delivery text-right">
				<span>{{ Language::trans('Dostava') }} </span>
				<span>{{ Language::trans('Besplatna dostava') }}</span>
			</div> 
			<div class="cart-final-price">
				<span>{{ Language::trans('Ukupno') }} </span>
				<span class="JSdelivery_total_amount sum-amount">{{Cart::cena(Cart::cart_ukupno())}}</span>
			</div>	
		</div>		
		
	</div>


	<div id="JSRegToggleSec"> 
	<!-- <span class="hidden"> {{ Session::has('b2c_kupac') ? "" : (count(Input::old()) == 0 ? "hidden='hidden'" : "") }}</span> -->
		
		<form method="POST" action="{{ Options::base_url() }}order-create" id="JSOrderForm" class="without-reg-form row">
			<div class="row"> 
				<div class="col-md-12 col-sm-12 col-xs-12 form-group">
					<h2 class="cart-heading">{{ Language::trans('2. Način isporuke') }}</h2>
					
					<ul class="delivery-method">
					@foreach(DB::table('web_nacin_isporuke')->where('selected',1)->get() as $key => $row)
						<li>
							<input type="radio" id="delivery-method-{{ $row->web_nacin_isporuke_id }}" name="web_nacin_isporuke_id" value="{{ $row->web_nacin_isporuke_id }}" class="JSdeliveryInput" {{ $key == 0 ? 'checked' : '' }}> <label class="flex" for="delivery-method-{{ $row->web_nacin_isporuke_id }}"><span class="delivery-span">{{ Language::trans($row->naziv) }}</span></label>
						</li>
					@endforeach
					</ul>
				</div>

				<div class="col-md-12 col-sm-12 col-xs-12 form-group">
					<h2 class="cart-heading">{{ Language::trans('3. Način plaćanja') }}</h2>

					<ul class="payment-method">
					@foreach(DB::table('web_nacin_placanja')->where('selected',1)->orderBy('b2c_default','desc')->get() as $key => $row)
						<li>
							<input type="radio" id="pay-method-{{ $row->web_nacin_placanja_id }}" name="web_nacin_placanja_id" value="{{ ($row->web_nacin_placanja_id) }}" {{ $key == 0 ? 'checked' : '' }}> <label class="flex" for="pay-method-{{ $row->web_nacin_placanja_id }}"><span class="payment-span">{{ Language::trans($row->naziv) }}</span></label>
						</li>
					@endforeach
					</ul>
				</div>
			</div>

			<h3 class="address-heading">{{ Language::trans('Informacije primaoca') }}</h3>

			<!-- CART ACTION BUTTONS -->
			<div class="cart-action-buttons text-right hidden" id="cart_form_scroll">
				@if(!Session::has('b2c_kupac'))
					@if(Options::neregistrovani_korisnici()==1)
					
					<button class="button" id="JSRegToggle">{{ Language::trans('Kupi bez registracije') }}</button>
					
					@endif

					<a class="button inline-block" href="#" role="button" id="login-icon" data-toggle="modal" data-target="#loginModal">{{ Language::trans('Prijavi se') }}</a>
				@endif
			</div> 

			@if(Options::neregistrovani_korisnici()==1 AND !Session::has('b2c_kupac'))
			<div class="row text-center hidden"> 
				
				@if(Input::old('flag_vrsta_kupca') == 1)
				<div class="col-md-6 col-sm-6 col-xs-6"> 
					<div class="JScheck_user_type without-btn personal" data-vrsta="personal">
						{{ Language::trans('Fizičko Lice') }} <i class="fas fa-check"></i> 
					</div>
				</div>

				<div class="col-md-6 col-sm-6 col-xs-6"> 
					<div class="JScheck_user_type without-btn active none-personal" data-vrsta="non-personal">
						{{ Language::trans('Pravno Lice') }} <i class="fas fa-check"></i>
					</div>
				</div>
				@else
				<div class="col-md-6 col-sm-6 col-xs-6"> 
					<div class="JScheck_user_type without-btn active personal" data-vrsta="personal">
						{{ Language::trans('Fizičko Lice') }} <i class="fas fa-check"></i>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-6"> 
					<div class="JScheck_user_type without-btn none-personal" data-vrsta="non-personal">
						{{ Language::trans('Pravno Lice') }} <i class="fas fa-check"></i>
					</div>
				</div>
				@endif 

			</div>
			@endif
			<input type="hidden" name="flag_vrsta_kupca" value="{{ Input::old('flag_vrsta_kupca') == 1 ? '1' : '0' }}">

			@if(Options::neregistrovani_korisnici()==1 AND !Session::has('b2c_kupac'))
			<div class="row"> 
				<div class="col-md-12 col-sm-12 col-xs-12 form-group JSwithout-reg-none-personal {{ Input::old('flag_vrsta_kupca') == 1 ? 'active' : '' }}">
					<label class="hidden" for="without-reg-company"><span class="red-dot-error"><i class="fas fa-star-of-life"></i></span> {{ Language::trans('Naziv Firme') }}:</label>
					<input placeholder="{{ Language::trans('Naziv Firme') }}" id="without-reg-company" name="naziv" type="text" tabindex="1" value="{{ htmlentities(Input::old('naziv') ? Input::old('naziv') : '') }}">
					<div class="error red-dot-error">{{ $errors->first('naziv') ? $errors->first('naziv') : "" }}</div>
				</div>

				<div class="col-md-3 col-sm-12 col-xs-12 form-group JSwithout-reg-none-personal {{ Input::old('flag_vrsta_kupca') == 1 ? 'active' : '' }}">
					<label class="hidden" for="without-reg-pib">{{ Language::trans('PIB') }}:</label>
					<input placeholder="{{ Language::trans('PIB') }}" id="without-reg-pib" name="pib" type="text" tabindex="2" value="{{ htmlentities(Input::old('pib') ? Input::old('pib') : '') }}">
					<div class="error red-dot-error">{{ $errors->first('pib') ? $errors->first('pib') : "" }}</div>
				</div>
				<div class="col-md-3 col-sm-12 col-xs-12 form-group JSwithout-reg-none-personal {{ Input::old('flag_vrsta_kupca') == 1 ? 'active' : '' }}">
					<label class="hidden" for="without-reg-maticni_br">{{ Language::trans('Matični broj') }}:</label>
					<input placeholder="{{ Language::trans('Matični broj') }}" id="without-reg-maticni_br" name="maticni_br" type="text" tabindex="2" value="{{ htmlentities(Input::old('maticni_br') ? Input::old('	') : '') }}">
					<div class="error red-dot-error">{{ $errors->first('maticni_br') ? $errors->first('maticni_br') : "" }}</div>
				</div>
				<div class="col-md-12 col-sm-12 col-xs-12 form-group JSwithout-reg-personal {{ Input::old('flag_vrsta_kupca') == 1 ? '' : 'active' }}">
					<label class="hidden" for="without-reg-name"><span class="red-dot-error"><i class="fas fa-star-of-life"></i></span> {{ Language::trans('Ime') }}</label>
					<input placeholder="{{ Language::trans('Ime') }}" id="without-reg-name" name="ime" type="text" tabindex="1" value="{{ htmlentities(Input::old('ime') ? Input::old('ime') : '') }}">
					<div class="error red-dot-error">{{ $errors->first('ime') ? $errors->first('ime') : "" }}</div>
				</div>

				<div class="col-md-12 col-sm-12 col-xs-12 form-group JSwithout-reg-personal {{ Input::old('flag_vrsta_kupca') == 1 ? '' : 'active' }}">
					<label class="hidden" for="without-reg-surname"><span class="red-dot-error"><i class="fas fa-star-of-life"></i></span> {{ Language::trans('Prezime') }}</label>
					<input placeholder="{{ Language::trans('Prezime') }}" id="without-reg-surname" name="prezime" type="text" tabindex="2" value="{{ htmlentities(Input::old('prezime') ? Input::old('prezime') : '') }}">
					<div class="error red-dot-error">{{ $errors->first('prezime') ? $errors->first('prezime') : "" }}</div>
				</div>

				<div class="col-md-12 col-sm-12 col-xs-12 form-group"> 
					<label class="hidden" for="without-reg-phone"><span class="red-dot-error"><i class="fas fa-star-of-life"></i></span> {{ Language::trans('Telefon') }}</label>
					<input placeholder="{{ Language::trans('Telefon') }}" id="without-reg-phone" name="telefon" type="text" tabindex="3" value="{{ htmlentities(Input::old('telefon') ? Input::old('telefon') : '') }}">
					<div class="error red-dot-error">{{ $errors->first('telefon') ? $errors->first('telefon') : "" }}</div>
				</div>

				<div class="col-md-12 col-sm-12 col-xs-12 form-group"> 
					<label class="hidden" for="without-reg-e-mail"><span class="red-dot-error"><i class="fas fa-star-of-life"></i></span> {{ Language::trans('E-mail') }}</label>
					<input placeholder="{{ Language::trans('E-mail') }}" id="JSwithout-reg-email" name="email" type="text" tabindex="4" value="{{ htmlentities(Input::old('email') ? Input::old('email') : '') }}">
					<div class="error red-dot-error">{{ $errors->first('email') ? $errors->first('email') : "" }}</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12 form-group"> 
					<label class="hidden" for="without-reg-address"><span class="red-dot-error"><i class="fas fa-star-of-life"></i></span> {{ Language::trans('Adresa za dostavu') }}</label>
					<input placeholder="{{ Language::trans('Adresa za dostavu') }}" id="without-reg-address" name="adresa" type="text" tabindex="5" value="{{ htmlentities(Input::old('adresa') ? Input::old('adresa') : '') }}">
					<div class="error red-dot-error">{{ $errors->first('adresa') ? $errors->first('adresa') : "" }}</div>
				</div>
				<div class="col-md-12 col-sm-12 col-xs-12 form-group"> 
					<label class="hidden" for="without-reg-address2"><span class="red-dot-error"><i class="fas fa-star-of-life"></i></span> {{ Language::trans('Dodatna adresa') }}</label>
					<input placeholder="{{ Language::trans('Dodatna adresa') }}" id="without-reg-address2" name="dodatna_adresa" type="text" tabindex="5" value="{{ htmlentities(Input::old('dodatna_adresa') ? Input::old('dodatna_adresa') : '') }}">
					<div class="error red-dot-error">{{ $errors->first('dodatna_adresa') ? $errors->first('dodatna_adresa') : "" }}</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12 form-group"> 
					<label class="hidden" for="without-reg-city"><span class="red-dot-error"><i class="fas fa-star-of-life"></i></span> {{ Language::trans('Mesto') }}/{{ Language::trans('Grad') }}</label>				 
					<input placeholder="{{ Language::trans('Mesto') }}/{{ Language::trans('Grad') }}" type="text" name="mesto" tabindex="6" value="{{ htmlentities(Input::old('mesto') ? Input::old('mesto') : '') }}">
					<div class="error red-dot-error">{{ $errors->first('mesto') ? $errors->first('mesto') : "" }}</div>
				</div>
				<div class="col-md-12 col-sm-12 col-xs-12 form-group"> 
					<label class="hidden" for="without-reg-code"><span class="red-dot-error"><i class="fas fa-star-of-life"></i></span> {{ Language::trans('Poštanski broj') }}</label>
					<input placeholder="{{ Language::trans('Poštanski broj') }}" id="without-reg-code" name="zip_kod" type="text" tabindex="5" value="{{ htmlentities(Input::old('zip_kod') ? Input::old('zip_kod') : '') }}">
					<div class="error red-dot-error">{{ $errors->first('zip_kod') ? $errors->first('zip_kod') : "" }}</div>
				</div>
			</div>
			@endif

			@if(Options::neregistrovani_korisnici()==1 OR Session::has('b2c_kupac'))
			@if(Session::has('b2c_kupac') AND Options::web_options(314)==1)
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12 form-group">
					<label>{{ Language::trans('Bodovi') }}</label>
					<div>{{ Language::trans('Ovom kupovinom broj bodova koji možete ostvariti je') }} <span id="JSAchievePoints">{{ Cart::bodoviOstvareniBodoviKorpa() }}</span>.</div>
					<div>{{ Language::trans('Broj bodova kiji imate je') }} {{ WebKupac::bodovi() }}.
						@if(!is_null($trajanjeBodova = WebKupac::trajanjeBodova()))
						{{ Language::trans('Rok važenja bodova je') }} {{ $trajanjeBodova }}.
						@endif
					</div>
					<div>{{ Language::trans('Maksimalni broj bodova kiji možete iskoristiti u ovoj kupovini je') }} <span id="JSMaxUsingPoints">{{ Cart::bodoviPopustBodoviKorpa() }}</span>.</div>
				</div>
				<div class="col-md-12 col-sm-12 col-xs-12 form-group">
					<label>{{ Language::trans('Unesite bodove koje želite da iskoristite') }}:</label>
					<input type="text" name="bodovi" tabindex="6" value="{{ htmlentities(Input::old('bodovi') ? Input::old('bodovi') : '') }}">
					<div class="error red-dot-error">{{ Session::has('bodovi_error') ? Session::get('bodovi_error') : '' }}</div>
				</div>
			</div>		
			@endif

			


			<div class="row"> 
				<div class="col-md-12 col-sm-12 col-xs-12 form-group hidden"> 
					<label>{{ Language::trans('Napomena') }}:</label>
					@if(Cart::kamata(Cart::korpa_id()) >0 )					
					<textarea rows="5" tabindex="9" name="napomena">{{ Language::trans('Kupovina na:') }} {{ Cart::kamata(Cart::korpa_id()) }} {{ Language::trans('rata') }}</textarea>
					@else
					<textarea rows="5" tabindex="9" name="napomena">{{ htmlentities(Input::old('napomena') ? Input::old('napomena') : '') }}</textarea>
					@endif
				</div>
				<div class="col-md-6 col-sm-6 col-md-offset-3 col-sm-offset-3 col-xs-12 form-group"> 
					<label class="hidden-xs">&nbsp;</label>
					<div class="capcha text-center"> 
						{{ Captcha::img(5, 160, 50) }}<br>
						<span>{{ Language::trans('Unesite kod sa slike') }}</span>
						<input type="text" name="captcha-string" tabindex="10" autocomplete="off">
						<div class="error red-dot-error">{{ $errors->first('captcha') ? $errors->first('captcha') : "" }}</div>
					</div>
				</div>
			</div>
			<div class="row hidden">
				<div class="col-md-12 col-sm-12 col-xs-12">  
					 <span class="red-dot-error"><i class="fas fa-star-of-life"></i></span> {{ Language::trans('Obavezna polja') }}
				</div>
			</div>
			@endif

			@if(Options::neregistrovani_korisnici()==1 OR Session::has('b2c_kupac'))
			<div class="text-center col-md-12 col-sm-12 col-xs-12 no-padding"> 
				<button id="JSOrderSubmit" class="button">{{ Language::trans('Završi kupovinu') }}</button>
			</div>
			@endif
		</form>	
	</div>

	@else

	<h2><span class="page-title">{{ Language::trans('Korpa') }}</span></h2>

	<div class="empty-page-label">{{ Language::trans('Vaša korpa je trenutno prazna') }}.</div>

	@endif
</div>


@endsection