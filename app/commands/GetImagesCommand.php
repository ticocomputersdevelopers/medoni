<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
// use Intervention\Image\Facades\Image;


class GetImagesCommand extends Command {
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'get:images';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Resize images.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	public static function getMappedArticles(){
		$mapped = array();
		$articles = DB::table('roba')->whereNotNull('id_is')->where('id_is','!=','')->get();
		foreach($articles as $article){
			$mapped[strval($article->sifra_is)] = $article->roba_id;
		}
		return $mapped;
	}
	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{	
		$mappedArticles = self::getMappedArticles();

		$folder = 'slike_za_sajt';
			//var_dump(glob($folder.'/*', GLOB_ONLYDIR));die;
		foreach(glob($folder.'/*', GLOB_ONLYDIR) as $dir) {
			$base = basename($dir);
			if ($base == '.' || $base == '..') continue;

			$images = File::files($folder.'/'.$base);

			foreach($images as $image){
				$imagePathArr = explode('/',$image);
				$imageNameArr = explode('.',$imagePathArr[count($imagePathArr)-1]);
				unset($imageNameArr[count($imageNameArr)-1]);
				$codeArr = explode('-',implode('.',$imageNameArr));
				$akcija = $codeArr[count($codeArr)-1] == '1' ? 1 : 0;
				unset($codeArr[count($codeArr)-1]);
				$code = implode('-',$codeArr);
				if(isset($mappedArticles[$code])){
					$roba_id = $mappedArticles[$code];
                    $slika_id = DB::select("SELECT nextval('web_slika_web_slika_id_seq')")[0]->nextval;
                    $name = $slika_id.'.jpg';
                    $putanja = 'images/products/big/'.$name;

                    if(File::copy($image,$putanja)){
	                    $insert_data = array(
	                        'web_slika_id'=>intval($slika_id),
	                        'roba_id'=> $roba_id,
	                        'akcija'=> $akcija, 
	                        'flag_prikazi'=>1,
	                        'putanja'=>$putanja
	                        );
                    	DB::table('web_slika')->insert($insert_data);
	                }
					
				}
			}
        }

		$this->info('Finish.');
	}

	public static function saveImageFile($putanja,$content,$sirina_big=600){
		try {
			$stream_content = stream_get_contents($content);
			$img_file = fopen($putanja,"wb");
			fwrite($img_file, $stream_content);
			fclose($img_file);

			if(filesize($putanja) <= 1000000){
				// Image::make($putanja)->resize($sirina_big, null, function ($constraint) { $constraint->aspectRatio(); })->save();	
			}
			return true;
		} catch (Exception $e) {
			File::delete($putanja);
			return false;
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			// array('transfer_domain', InputArgument::OPTIONAL, 'Image path on root.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
		// 	array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}