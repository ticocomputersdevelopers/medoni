<?php
namespace Service;
use Options;
use DB;

class MSUApi {

	public static function getPayload($web_b2c_narudzbina_id){
        $web_b2c_narudzbina = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->first();
        if(is_null($web_b2c_narudzbina)){
            return array();
        }
        $web_kupac = DB::table('web_kupac')->where('web_kupac_id',$web_b2c_narudzbina->web_kupac_id)->first();
        if(is_null($web_kupac)){
            return array();
        }
        $preduzece = DB::table('preduzece')->where('preduzece_id',1)->first();
        if(is_null($preduzece)){
            return array();
        }
        $web_b2c_narudzbina_stavke = DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->get();
        $countStavke = count($web_b2c_narudzbina_stavke);
        if($countStavke == 0){
            return array();
        }

        $items = "[";
        $amount = 0;
        foreach($web_b2c_narudzbina_stavke as $key => $stavka){
        	$naziv = preg_replace('/[^a-z0-9 ]/i','',substr(DB::table('roba')->where('roba_id',$stavka->roba_id)->pluck('naziv_web'),0,60));
        	$item = array(
        		"code" => $stavka->roba_id,
        		"name" => $naziv,
        		"description" => "",
        		"quantity" => intval(round($stavka->kolicina)),
        		"amount" => sprintf('%0.2f',strval(round($stavka->jm_cena,2)))
        	);
        	$items .= "\n".json_encode($item).(($countStavke-1) > $key ? "," : "");
        	$amount += $stavka->jm_cena*$stavka->kolicina;
        }
        $items .= "\n]";

		$merchant = 'chipcardtest01';
		$merchantuser = 'api.test@payten.com';
		$merchantpassword = 'Hephr=R4SKNycaLf';
		return array(
			'ACTION' => 'SESSIONTOKEN',
			'MERCHANTUSER' => $merchantuser,
			'MERCHANTPASSWORD' => $merchantpassword,
			'MERCHANT' => $merchant,
			'CUSTOMER' => preg_replace('/[^a-z0-9]/i','_',strtolower($preduzece->naziv)).'_'.strval($web_kupac->web_kupac_id),
			'SESSIONTYPE' => 'PAYMENTSESSION',
			'MERCHANTPAYMENTID' => strval($web_b2c_narudzbina_id),
			'AMOUNT' => sprintf('%0.2f',strval(round($amount,2))),
			'CURRENCY' => 'RSD',
			'CUSTOMEREMAIL' => $web_kupac->email,
			'CUSTOMERNAME' => $web_kupac->flag_vrsta_kupca == 0 ? $web_kupac->ime.' '.$web_kupac->prezime : $web_kupac->naziv.' '.$web_kupac->pib,
			'CUSTOMERPHONE' => $web_kupac->telefon,
			'RETURNURL' => Options::base_url().'msu-response',
			'SESSIONTYPE' => 'PAYMENTSESSION',
			'EXTRA' => '',
			'ORDERITEMS' => $items
		);
	}

	public static function getSessionToken($payload){
		$url = "https://entegrasyon.asseco-see.com.tr/msu/api/v2";

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
		$response = curl_exec($ch);
		$result = json_decode($response);

		if($result->responseCode == '00' AND $result->responseMsg == 'Approved'){
			return $result->sessionToken;
		}
		return null;
	}
}