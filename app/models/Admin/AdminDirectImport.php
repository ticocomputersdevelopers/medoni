<?php
use DirectImport\ReadFile;
use DirectImport\Article;
use DirectImport\Lager; 
use DirectImport\Group;
use DirectImport\Osobine;
use DirectImport\Images; 
use DirectImport\Support; 

class AdminDirectImport {

    public static function xml_execute(){
        $result = ReadFile::xml();
        if($result->success){        
            self::execute($result->articles);
        }
        return $result->success;
    }

    public static function xls_execute(){
        $result = ReadFile::xls();
        if($result->success){        
            self::execute($result->articles);
        }
        return $result->success;
    }
 
    public static function xlsx_execute(){
        $result = ReadFile::xls(true);
        if($result->success){        
            self::execute($result->articles);
        }
        return $result->success;
    }
    public static function xls_execute_partner(){
        $result = ReadFile::xls_partner();
        if($result->success){        
            self::execute_partner($result->partners);
        }
        return $result->success;
    }

    public static function xlsx_execute_partner(){
        $result = ReadFile::xls_partner(true);
        if($result->success){        
            self::execute_partner($result->partners);
        }
        return $result->success;
    }


    public static function csv_execute(){
        $result = ReadFile::csv();
        if($result->success){        
            self::execute($result->articles);
        }
        return $result->success;
    }
    public static function csv_execute_vaucer(){
        $result = ReadFile::csv();
        if($result->success){        
            self::execute($result->articles);
        }
        return $result->success;
    }

    public static function execute($articles){

       //  //grupe
       //  $groups=ReadFile::groups();
       // // All::dd($groups);die;
       //  $resultGroup = Group::table_body($groups);
       //  Group::query_insert_update($resultGroup->body);

       //  Support::updateGroupsParent();
  
        //articles
        $resultArticle = Article::table_body($articles);
        if($resultArticle->body!=''){
            Article::query_insert_update($resultArticle->body,array('sifra_is','naziv','naziv_displej','web_cena','naziv_web','osobine','web_opis','proizvodjac_id'));
            // Article::query_update_unexists($resultArticle->body);
        }
        $dodatne_grupe = Article::dodatneGrupeUpdate($articles);
        //osobine
        $resultOsobine = Osobine::table_body($articles);
        if($resultOsobine->osobina_kombinacija !=''){
            Osobine::query_insert_update($resultOsobine);
            Osobine::update_osobina_kombinacija_vrednost($articles);
        }
    
        //lager
        $resultLager = Lager::table_body($articles);
        if($resultLager->body!=''){
            Lager::query_insert_update($resultLager->body,array('kolicina','sifra_is'));
        }
       // var_dump($articles);die;
        // images
        // $resultImage = Images::table_body($articles);
        // if($resultImage->body!=''){
        //     Images::query_insert_update($resultImage->body);
        // }
    }
    public static function execute_partner($partners){
        //articles
        $resultPartners = Article::table_body_partner($partners);
        Article::query_insert_update_partners($resultPartners->body,array('partner_id','naziv','adresa','mesto','telefon','fax','pib','broj_maticni','mail','komercijalista'));

        
    }


}