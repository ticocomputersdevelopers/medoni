<?php
namespace DirectImport;
use DB;

class Lager {

	public static function table_body($articles){

		$result_arr = array();
		//$roba_id = -1;
		foreach($articles as $article) {
    		$roba_id = DB::table('roba')->where('sifra_is',$article->code)->pluck('roba_id');

			$sifra = pg_escape_string($article->code);
			$kolicina = intval($article->quantity);
			if($kolicina < 0){
				$kolicina = 0;
			}

			$result_arr[] = "(".strval($roba_id).",1,0,0,0,".strval($kolicina).",0,0,0,0,0,0,0,0,(NULL)::integer,0,0,0,0,0,2014,-1,0,0,'".strval($sifra)."')";

		}
		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {

		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='lager'"));
		$table_temp = "(VALUES ".$table_temp_body.") lager_temp(".implode(',',$columns).")";

		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		// update
		$updated_columns=array();
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="roba_id"){
		    	$updated_columns[] = "".$col." = lager_temp.".$col."";
			}
		}
		
		DB::statement("UPDATE lager t SET kolicina = lager_temp_grouped.kolicina FROM (SELECT MAX(orgj_id) AS orgj_id, SUM(kolicina) AS kolicina, MAX(roba_id) AS roba_id FROM ".$table_temp." GROUP BY roba_id, orgj_id, poslovna_godina_id) AS lager_temp_grouped WHERE t.roba_id=lager_temp_grouped.roba_id AND t.orgj_id=lager_temp_grouped.orgj_id");

		$where = "";
		if(DB::table('lager')->count() > 0){
			$where .= " WHERE (roba_id, orgj_id, poslovna_godina_id) NOT IN (SELECT roba_id, orgj_id, poslovna_godina_id FROM lager)";
		}

		//insert
		DB::statement("INSERT INTO lager (roba_id, orgj_id, kolicina, poslovna_godina_id) SELECT roba_id, orgj_id, SUM(kolicina), poslovna_godina_id FROM ".$table_temp.$where." GROUP BY roba_id, orgj_id, poslovna_godina_id ");
		//DB::statement("INSERT INTO lager (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM lager t WHERE t.sifra_is=lager_temp.sifra_is::varchar))");
		

		//insert

		//DB::statement("DELETE FROM lager WHERE sifra_is = (SELECT sifra_is FROM lager GROUP BY sifra_is HAVING COUNT(sifra_is) > 1)");
		//DB::statement("UPDATE lager l SET roba_id = roba.roba_id FROM roba WHERE l.sifra_is=roba.sifra_is");
		//DB::statement("DELETE FROM lager WHERE roba_id < 0");

	}

}