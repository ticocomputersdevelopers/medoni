<?php
namespace Import;
use Import\Support;
use DB;
use File;
use PHPExcel; 
use PHPExcel_IOFactory;

class Cudo {

	public static function execute($dobavljac_id,$extension=null){
		if($extension==null){
			$products_file = "files/cudo/cudo_excel/cudo.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();
	        

	       	        for ($row = 1; $row <= $lastRow; $row++) {
	            $sifra = $worksheet->getCell('A'.$row)->getValue();
	            $naziv = $worksheet->getCell('C'.$row)->getValue();
				$cena = $worksheet->getCell('D'.$row)->getValue();
				$pmp_cena = $worksheet->getCell('E'.$row)->getValue();
				$opis = $worksheet->getCell('I'.$row)->getValue();
				$barkod = $worksheet->getCell('F'.$row)->getValue();
				$proizvodjac = $worksheet->getCell('G'.$row)->getValue();
				$grupa = $worksheet->getCell('B'.$row)->getValue();
				$slika = "http://local.b2c/images/cudo/".$sifra.".jpg";
				
				if(isset($sifra) && isset($naziv) && isset($cena) && is_numeric($cena)){
// var_dump($slika);die;
					$naziv = strtolower($naziv);
					$naziv = ucfirst($naziv);
					$naziv = str_replace("Š", "š", $naziv);$naziv = str_replace("Č", "č", $naziv);$naziv = str_replace("Ć", "ć", $naziv);$naziv = str_replace("Ž", "ž", $naziv);


					$sPolja = '';
					$sVrednosti = '';
					$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
					$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . addslashes(Support::encodeTo1250($sifra)) ."',";
					$sPolja .= "naziv,";					$sVrednosti .= "'" . addslashes(Support::encodeTo1250($proizvodjac)) ." " . Support::encodeTo1250($naziv) ." ',";
					$sPolja .= "opis,";	     				$sVrednosti .= "'" . Support::encodeTo1250($opis) ." ',";
					$sPolja .= "grupa,";	     			$sVrednosti .= "'" . Support::encodeTo1250($grupa) ." ',";
					$sPolja .= "proizvodjac,";	     		$sVrednosti .= "'" . addslashes(Support::encodeTo1250($proizvodjac)) ." ',";
					$sPolja .= "barkod,";	     		    $sVrednosti .= "'" . addslashes(Support::encodeTo1250($barkod)) ." ',";
					$sPolja .= "flag_slika_postoji,";		$sVrednosti .= "1,";
					$sPolja .= "flag_opis_postoji,";		$sVrednosti .= "1,";
					$sPolja .= "kolicina,";					$sVrednosti .= "1,";
					$sPolja .= "pmp_cena,";					$sVrednosti .= "" . $pmp_cena . ",";		
					$sPolja .= "cena_nc";					$sVrednosti .= "" . $cena . "";
			
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
					DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp (partner_id,sifra_kod_dobavljaca,putanja,akcija) VALUES(".$dobavljac_id.",'".$sifra."','".$slika."',1 )");	


				}

			}
			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array('i'));
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$extension=null){

		if($extension==null){
			$products_file = "files/cudo/cudo_excel/cudo.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();

	        for ($row = 1; $row <= $lastRow; $row++) {
	            $sifra = $worksheet->getCell('A'.$row)->getValue();
	            $naziv = $worksheet->getCell('B'.$row)->getValue();
				$cena = $worksheet->getCell('E'.$row)->getValue();

				if(isset($sifra) && isset($naziv) && isset($cena) && is_numeric($cena)){

					$sPolja = '';
					$sVrednosti = '';
					$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
					$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . addslashes(Support::encodeTo1250($sifra)) ."',";
					$sPolja .= "kolicina,";					$sVrednosti .= "1,";		
					$sPolja .= "cena_nc";					$sVrednosti .= "" . $cena*0.85 . "";
			
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");	

				}

			}
			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}


}