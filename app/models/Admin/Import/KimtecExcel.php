<?php
namespace Import;
use Import\Support;
use DB;
use File;
use PHPExcel; 
use PHPExcel_IOFactory;

class KimtecExcel {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){
		if($extension==null){
			$products_file = "files/kimtec/kimtec_excel/kimtec.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();
	        
	        for ($row = 1; $row <= $lastRow; $row++) {
	        	$sifra = $worksheet->getCell('A'.$row)->getValue();
	        	$lager = $worksheet->getCell('L'.$row)->getValue();
	        	$naziv = $worksheet->getCell('F'.$row)->getValue();
	        	$proizvodjac = $worksheet->getCell('E'.$row)->getValue();
	        	$grupa = $worksheet->getCell('C'.$row)->getValue();
	        	$podgrupa = $worksheet->getCell('D'.$row)->getValue();
	        	$cena_nc = $worksheet->getCell('K'.$row)->getValue();

			 	if(!empty($sifra) && !empty($naziv) && !empty($cena_nc) && is_numeric($cena_nc)){
			 		if($lager=='L' || $lager=='M' || $lager=='PN'){
			 			$kolicina=1;
			 		}else{
			 			$kolicina=0;
			 		}

					$sPolja = '';
					$sVrednosti = '';

					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . addslashes($sifra) . "',";
					$sPolja .= " naziv,";					$sVrednosti .= " '" . addslashes(Support::encodeTo1250($naziv)) . "',";
					if(!empty($proizvodjac)){
					$sPolja .= " proizvodjac,";				$sVrednosti .= " '" . addslashes(Support::encodeTo1250($proizvodjac)) . "',";
					}
					if(!empty($grupa)){
					$sPolja .= " grupa,";					$sVrednosti .= " '" . addslashes(Support::encodeTo1250($grupa)) . "',";
					}
					if(!empty($podgrupa)){
					$sPolja .= " podgrupa,";				$sVrednosti .= " '" . addslashes(Support::encodeTo1250($podgrupa)) . "',";
					}
					$sPolja .= " kolicina,";				$sVrednosti .= " " . $kolicina . ",";
					$sPolja .= " cena_nc";					$sVrednosti .= " " . Support::replace_empty_numeric($cena_nc,2,$kurs,$valuta_id_nc). "";

					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

				}
			}
			Support::queryExecute($dobavljac_id,array('i','u'),array(),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			$products_file = "files/kimtec/kimtec_excel/kimtec.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();

	        for ($row = 1; $row <= $lastRow; $row++) {
	        	$sifra = $worksheet->getCell('A'.$row)->getValue();
	        	$lager = $worksheet->getCell('L'.$row)->getValue();
	        	$cena_nc = $worksheet->getCell('K'.$row)->getValue();

			 	if(!empty($sifra) && !empty($cena_nc) && is_numeric($cena_nc)){
			 		if($lager=='L' || $lager=='M' || $lager=='PN'){
			 			$kolicina=1;
			 		}else{
			 			$kolicina=0;
			 		}

					$sPolja = '';
					$sVrednosti = '';

					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . addslashes($sifra) . "',";
					$sPolja .= " kolicina,";				$sVrednosti .= " " . $kolicina . ",";
					$sPolja .= " cena_nc";					$sVrednosti .= " " . Support::replace_empty_numeric($cena_nc,2,$kurs,$valuta_id_nc). "";

					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

				}
			}

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}


}