<?php
namespace Import;
use Import\Support;
use DB;
use File;
use PHPExcel; 
use PHPExcel_IOFactory;

class EuromDenis {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){
		if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/euromdenis/euromdenis_csv/euromdenis.csv');
			$products_file = "files/euromdenis/euromdenis_csv/euromdenis.csv";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}
			
			$i = 0;
			$handle = fopen($products_file, "r");
		
			while (($data = fgetcsv($handle,10000,";")) !== FALSE) {
		
			$sifra = $data[0];
			$ean = $data[1];
 			//var_dump($data[1]);die();
 			$flag_slika_postoji = "0";

 				if(isset($data[2])){
					if(isset($data[12])){						
						DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",'".$sifra."','".$data[12]."',1 )");
					$flag_slika_postoji = "1";
					}	
		
					if(!empty($data[2])){
					$sPolja = '';
					$sVrednosti = '';					
					
					$sPolja .= "partner_id,";					$sVrednosti .= "" . $dobavljac_id . ",";
					$sPolja .= "sifra_kod_dobavljaca,";			$sVrednosti .= "'" . addslashes($sifra) . "',";
					$sPolja .= "model,";						$sVrednosti .= "'" . addslashes($sifra) . "',";
					$sPolja .= "naziv,";						$sVrednosti .= "'" . pg_escape_string($data[2]) . " ( ".$sifra." )". "',";
					$sPolja .= "opis,";							$sVrednosti .= "'" . pg_escape_string($data[11]) . "',";
					$sPolja .= "kolicina,";						$sVrednosti .= "'" . number_format(intval($data[6]), 2, '.', '') . "',";
					$sPolja .= "cena_nc,";						$sVrednosti .= "" . number_format(floatval(Support::replace_empty_numeric(str_replace(',', '.', $data[3]),1,$kurs,$valuta_id_nc)), 2, '.', '') . ",";
					$sPolja .= "pmp_cena,";						$sVrednosti .= "" . number_format(floatval(Support::replace_empty_numeric(str_replace(',', '.', $data[4]),1,$kurs,$valuta_id_nc)), 2, '.', '') . ",";	
					$sPolja .= "web_flag_karakteristike,";		$sVrednosti .= " 0,";
					$sPolja .= "flag_slika_postoji";			$sVrednosti .= "" . $flag_slika_postoji . "";
										
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
					}
				}	
			}	
		}
			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){
		if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/euromdenis/euromdenis_csv/euromdenis.csv');
			$products_file = "files/euromdenis/euromdenis_csv/euromdenis.csv";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}
			
			$i = 0;
			$handle = fopen($products_file, "r");
		
			while (($data = fgetcsv($handle,10000,";")) !== FALSE) {		
		
			$sifra = $data[0];
			$ean = $data[1];
 			//var_dump($data[5]);die();
 			$flag_slika_postoji = "0";

 				if(isset($data[2])){
		
					$sPolja = '';
					$sVrednosti = '';					
				
					$sPolja .= "partner_id,";					$sVrednosti .= "" . $dobavljac_id . ",";
					$sPolja .= "sifra_kod_dobavljaca,";			$sVrednosti .= "'" . addslashes($sifra) . "',";
					$sPolja .= "kolicina,";						$sVrednosti .= "'" . number_format(intval($data[6]), 2, '.', '') . "',";
					$sPolja .= "cena_nc,";						$sVrednosti .= "" . number_format(floatval(Support::replace_empty_numeric(str_replace(',', '.', $data[3]),1,$kurs,$valuta_id_nc)), 2, '.', '') . ",";
					$sPolja .= "pmp_cena";						$sVrednosti .= "" . number_format(floatval(Support::replace_empty_numeric(str_replace(',', '.', $data[4]),1,$kurs,$valuta_id_nc)), 2, '.', '') . "";	
										
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
				}	
			}	
		}

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
	}
	

	public static function improve_file($putanja){
		$str=file_get_contents($putanja);
		$str=str_replace(array("Ugovorena-cena","Preporucena-cena"), array("Ugovorena_cena","Preporucena_cena"), $str);
		file_put_contents($putanja, $str);
	}

	public static function convertToUTF8($text){

	    $encoding = mb_detect_encoding($text, mb_detect_order(), false);
	    if($encoding == "UTF-8")
	    {
	        $text = mb_convert_encoding($text, 'UTF-8', 'UTF-8');    
	    }
	    $out = iconv(mb_detect_encoding($text, mb_detect_order(), false), "UTF-8//IGNORE", $text);
	    return $out;
	}

}