<?php

class PartnerApi extends Eloquent
{
    protected $table = 'partner';

    protected $primaryKey = 'partner_id';
    
    public $timestamps = false;

	protected $hidden = array(
		'partner_id',
        'sifra',
        'naziv',
        'adresa',
        'mesto',
        'drzava_id',
        'telefon',
        'fax',
        'pib',
        'broj_maticni',
        'broj_registra',
        'broj_resenja',
        'delatnost_sifra',
        'delatnost_naziv',
        'rabat',
        'limit_p',
        'dani_valute',
        'naziv_puni',
        'slanje_cenovnik',
        'slanje_news',
        'tip_cene_id',
        'valuta_id',
        'flag_crna_lista',
        'login',
        'password',
        'racun',
        'napomena',
        'pristup_kategoriji',
        'u_pdv',
        'stara_sifra',
        'stari_racun',
        'mail',
        'sifra_connect',
        'promena',
        'flag_promena_connect', 
        'imenik_id',
        'komercijalista',
        'preracunavanje_cena',
        'id_is',
        'ulica_id',
        'broj',
        'kontakt_osoba',
        'id_kategorije',
        'api_username',
        'api_password',
        'aktivan_api',
        'parent_id',
        'ptt_cityexpress',
        'datum_kreiranja'
    	);

	public function getIdAttribute()
	{
	    return isset($this->attributes['partner_id']) ? $this->attributes['partner_id'] : null;
	}
    public function getNameAttribute()
    {
        return isset($this->attributes['naziv']) ? $this->attributes['naziv'] : null;
    }
    public function getAddressAttribute()
    {
        return isset($this->attributes['adresa']) ? $this->attributes['adresa'] : null;
    }
    public function getCityAttribute()
    {
        return isset($this->attributes['mesto']) ? $this->attributes['mesto'] : null;
    }
    public function getPhoneAttribute()
    {
        return isset($this->attributes['telefon']) ? $this->attributes['telefon'] : null;
    }
    public function getEmailAttribute()
    {
        return isset($this->attributes['mail']) ? $this->attributes['mail'] : null;
    }
    public function getTaxIdAttribute()
    {
        return isset($this->attributes['pib']) ? intval(trim($this->attributes['pib'])) : null;
    }
    public function getCompanyRegistrationNumberAttribute()
    {
        return isset($this->attributes['broj_maticni']) ? intval(trim($this->attributes['broj_maticni'])) : null;
    }
    public function getBankAccountNumberAttribute()
    {
        return isset($this->attributes['racun']) ? $this->attributes['racun'] : null;
    }
    public function getRebateAttribute()
    {
        return isset($this->attributes['rabat']) ? floatval($this->attributes['rabat']) : null;
    }
    public function getExternalCodeAttribute()
    {
        return isset($this->attributes['id_is']) && !empty($this->attributes['id_is']) ? $this->attributes['id_is'] : null;
    }


	protected $appends = array(
    	'id',
    	'name',
        'address',
        'city',
        'phone',
        'email',
        'tax_id',
        'company_registration_number',
        'bank_account_number',
        'rebate',
        'external_code'
    	);

    public function cards(){
        return $this->hasMany('PartnerCard','partner_id');
    }

    public function mappedByExternalCode(){
        $mapped = [];
        foreach(self::select('partner_id','id_is')->whereNotNull('id_is')->where('id_is','<>','')->get() as $category){
            $mapped[$category->id_is] = $category->partner_id;
        }
        return $mapped;
    }

}