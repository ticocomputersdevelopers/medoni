<?php


class AdminFrontController extends Controller {

    function front_admin_save()
    {
        $change_data = Input::get('change_data');
        $lang_id = Language::lang_id();
        
        foreach($change_data as $data){
            if($data['action'] == 'sale'){
                AdminFront::saveSale($data['content']);
                AdminSupport::saveLog('FRONT_IZMENA_PRODAJA');
            }elseif($data['action'] == 'blogs'){
                AdminFront::saveBlogs($data['content']);
                AdminSupport::saveLog('FRONT_IZMENA_VESTI');
            }elseif($data['action'] == 'home_all_articles'){
                AdminFront::saveAllArticles($data['content']);
                AdminSupport::saveLog('FRONT_IZMENA_SVI_ARTIKLI');
            }elseif($data['action'] == 'type'){
                AdminFront::saveType($data['content'],$data['id']);
                AdminSupport::saveLog('FRONT_IZMENA_TIP');
            }elseif($data['action'] == 'slide_title'){
                AdminFront::saveSlideTitle($data['content'],$data['id'],$lang_id);
                AdminSupport::saveLog('FRONT_IZMENA_NASLOV_SLAJDER');
            }elseif($data['action'] == 'slide_pretitle'){
                AdminFront::saveSlidePreTitle($data['content'],$data['id'],$lang_id);
                AdminSupport::saveLog('FRONT_IZMENA_PODNASLOV_SLAJDER');
            }elseif($data['action'] == 'slide_content'){
                AdminFront::saveSlideContent($data['content'],$data['id'],$lang_id);
                AdminSupport::saveLog('FRONT_IZMENA_SADRZAJ_SLAJDER');
            }elseif($data['action'] == 'slide_button'){
                AdminFront::saveSlideButton($data['content'],$data['id'],$lang_id);
                AdminSupport::saveLog('FRONT_IZMENA_DUGME_SLAJDER');
            }elseif($data['action'] == 'footer_section_label'){
                AdminFront::saveFooterSectionLabel($data['content'],$data['id'],$lang_id);
                AdminSupport::saveLog('FRONT_IZMENA_FUTER_NASLOV');
            }elseif($data['action'] == 'footer_section_content'){
                AdminFront::saveFooterSectionContent($data['content'],$data['id'],$lang_id);
                AdminSupport::saveLog('FRONT_IZMENA_FUTER_SADRZAJ');
            }elseif($data['action'] == 'newslatter_label'){
                AdminFront::saveNewslatterLabel($data['content'],$lang_id);
                AdminSupport::saveLog('FRONT_IZMENA_BROSURA');
            }elseif($data['action'] == 'newslatter_content'){
                AdminFront::saveNewslatterContent($data['content'],$lang_id);
                AdminSupport::saveLog('FRONT_IZMENA_SADRZAJ_BROSURA');
            }elseif($data['action'] == 'front_admin_label'){
                AdminFront::saveFrontAdminLabel($data['content'],$data['id']);
                AdminSupport::saveLog('FRONT_IZMENA_NASLOV_SADRZAJ');
            }elseif($data['action'] == 'front_admin_content'){
                AdminFront::saveFrontAdminContent($data['content'],$data['id'],$lang_id);
                AdminSupport::saveLog('FRONT_IZMENA_SADRZAJ');
            }elseif($data['action'] == 'page_section'){
                AdminFront::saveFrontAdminPageSectionName($data['content'],$data['id'],$lang_id);
                AdminSupport::saveLog('FRONT_IZMENA_SEKCIJE_STRANICE');
            }

        }
    }
}