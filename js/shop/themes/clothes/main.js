
$(document).ready(function () {
// LAZY LOAD IMAGES
	$(function() {
		// change data-src to src to disable plugin
		// 1.product_on_grid
		// 2.product_on_list
		// 3.footer
        // $('.JSlazy_load').Lazy();
    });

	  // IF THERE IS NO IMAGE
	$('img').on('error', function(){ 
		$(this).attr('src', '/images/no-image.jpg').addClass('img-responsive');
	});

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

// *********************************************************  

 
 
// REMOVE EMPTY LOGGED USER
$('.logged-user').each(function(i, el){ 
	if ( ($(el).text().trim() == '')) {
		$(el).hide();
	}
});


// SLICK SLIDER INCLUDE
if ($('.JSmain-slider').length) {
	if($('#admin_id').val()){

		$('.JSmain-slider').slick({
			autoplay: false,
			draggable: false
		});

	}else{
		$('.JSmain-slider').slick({
			autoplay: true
		});
	} 
}
 
// PRODUCTS SLICK SLIDER 
if ($('.JSproducts_slick').length){ 
	$('.JSproducts_slick').slick({ 
		autoplay: true,
		slidesToShow: 4,
		arrows: true,
		slidesToScroll: 1, 
		responsive: [
			{
			  breakpoint: 1160,
			  settings: {
				slidesToShow: 3,
				slidesToScroll: 3
			  }
			},
			{
			  breakpoint: 768,
			  settings: {
				slidesToShow: 3,
				slidesToScroll: 3
			  }
			},
			{
			  breakpoint: 480,
			  settings: {
				slidesToShow: 2,
				slidesToScroll: 2,
				arrows: false
			  }
			}
		]
	}); 
}

 
// RELATED ARTICLES TITLE 
	$('.slickTitle').each(function(i, tit){
		if($(tit).next('.JSproducts_slick').find('.slick-track').children('.JSproduct ').length){
			$(tit).show();
		}
	})
  
	// if ($('.JSBrandSlider')[0]){
	// 	$('.JSBrandSlider').slick({
	// 		autoplay: true,
	// 	    infinite: true,
	// 	    speed: 600,
	// 	    arrows: true,
	// 		slidesToShow: 5,
	// 		slidesToScroll: 3, 
	// 		responsive: [
	// 			{
	// 			  breakpoint: 1100,
	// 			  settings: {
	// 				slidesToShow: 3,
	// 				slidesToScroll: 3,
	// 				infinite: true,
	// 				dots: false
	// 			  }
	// 			},
	// 			{
	// 			  breakpoint: 800,
	// 			  settings: {
	// 				slidesToShow: 2,
	// 				slidesToScroll: 2
	// 			  }
	// 			},
	// 			{
	// 			  breakpoint: 480,
	// 			  settings: {
	// 				slidesToShow: 2,
	// 				slidesToScroll: 2
	// 			  }
	// 			}
	// 		]
	// 	});
	// }

	if ($('.JSblog-slick').length){

		$('.JSblog-slick').slick({
			autoplay: true,
		    infinite: true,
		    speed: 600, 
			slidesToShow: 3,
			slidesToScroll: 1, 
			responsive: [
				{
				  breakpoint: 1100,
				  settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
					infinite: true,
					dots: false
				  }
				},
				{
				  breakpoint: 800,
				  settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
					arrows: false,
				  }
				},
				{
				  breakpoint: 480,
				  settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					arrows: false,
				  }
				}
			]
		});
	}
	  
 
	// CATEGORIES - MOBILE  
	
	$('.JSlevel-1 li').each(function(i, li){
		if($(li).children('ul').length){
			$(li).append('<span class="JSsubcategory-toggler"><i class="sprite sprite-down"></i></span>');
		}
	});          

	$('.JSsubcategory-toggler').on('click', function() { 

		if ($(this).siblings('ul').css('display') == 'none') {
			$(this).children().removeClass('sprite-down').addClass('sprite-up'); 
			$(this).parent().siblings().find('span i').removeClass('sprite-up').addClass('sprite-down');  
		} else {
			$(this).children().removeClass('sprite-up').addClass('sprite-down');
		}
 
	    $(this).siblings('ul').slideToggle();
		$(this).parent().siblings().children('ul').slideUp(); 
	});  
	
 
 // SOCIAL ICONS
	$('.social-icons .facebook').append('<i class="fab fa-facebook-f"></i>');
	$('.social-icons .twitter').append('<i class="fab fa-twitter"></i>');
	$('.social-icons .google-plus').append('<i class="fab fa-google-plus-g"></i>');
	$('.social-icons .skype').append('<i class="fab fa-skype"></i>');
	$('.social-icons .instagram').append('<i class="fab fa-instagram"></i>');
 	$('.social-icons .linkedin').append('<i class="fab fa-linkedin"></i>');
	$('.social-icons .youtube').append('<i class="fab fa-youtube"></i>'); 
 
 
 // GALLERY SLIDER
	(function(){
 		var img_gallery_href = $('.JSimg-gallery'),
 			img_gallery_img = img_gallery_href.children('img'),
 			main_img = $('.JSmain_img'),
 			modal_img = $('#JSmodal_img'),
 			modal = $('.JSmodal'),

 			img_arr = [], 
			next_prev = 0;

		img_gallery_href.eq(0).addClass('galery_Active');

		img_gallery_href.on('click', function(){
			var img = $(this).children('img');
			main_img.attr({ src: img.attr('src'), alt: img.attr('alt') });
			$(this).addClass('galery_Active').siblings().removeClass('galery_Active');
		});

		

		main_img.on('click', function(){
			var src = $(this).attr('src');
			if (src.toLowerCase().indexOf('no-image') <= 0){
				modal_img.attr('src', src);

				$('.JSmodal').show();

				//IF MAIN PHOTO (WHEN CLICKED ON IT) IS EQUAL TO ANY OF THE THUMBNAILS, FIND HER
				// ORDINAL NUMBER AND THEN ADD THAT NUMBER TO THE NEXT PREV

				img_gallery_img.each(function(i, img){
					img_arr.push($(img).attr('src'));  
	    			if( src ===  $(img).attr('src')) {
						next_prev = img_gallery_img.index(this);
	    			}
					
				});
			} 
		});

		$('.JSclose-modal').on('click', function(){
			modal.hide();
		});

		$(document).on('click', function(){
			if (modal.css('display') == 'block')  {
				$('body').css('overflow', 'hidden');
			} else {
				$('body').css('overflow', '');  
			} 
		});

		modal.on('click', function(e){
			if ( ! $('.JSmodal img, .JSmodal .btn, .JSmodal .btn > *').is(e.target)) {
				modal.hide();
				modal_img.attr('src', '');
			} 
		}); 

		img_gallery_img.each(function(i, img){
			img_arr.push($(img).attr('src'));  
		});


		$(document).on('click','.JSleft_btn',function(){
	        if (next_prev == 0) {
	          next_prev = img_gallery_img.length -1; 
	        } else {
	          next_prev --;
	          modal_img.attr('src', img_arr[next_prev]);
	        } 
	        modal_img.attr('src', img_arr[next_prev]);
	    });  
        
	    $(document).on('click','.JSright_btn',function(){

	        if (next_prev != img_gallery_img.length -1) {
	            next_prev++; 
	          } else {
	            next_prev = 0;
	        } 
	        modal_img.attr('src', img_arr[next_prev]);

	    });  


		if (img_gallery_href.length > 1) {
			$('.modal-cont .btn').show();
		}
 
	}());
 
// ======== SEARCH ============
 
	var searchTime;
	$('#JSsearch2').on("keyup", function(event) {
		if(event.keyCode == 13 && $('#JSsearch2').val() != ''){
        	$(".JSsearch-button2").trigger("click");
    	}
    	if($('#elasticsearch').val() == 1){
			timer2();
    	}else{	
			clearTimeout(searchTime);
			searchTime = setTimeout(timer2, 300);
    	}
	});
 
	$('.JSSearchGroup2').change(function(){	timer2(); });

	function timer2(){
		var search_length = 2;
		if($('#elasticsearch').val() == 1){
			search_length = 0;
		}
		var search = $('#JSsearch2').val();
		if (search != '' && search.length > search_length) {
			$.post(base_url + 'livesearch', {search:search, grupa_pr_id: $('.JSSearchGroup2').val()}, function (response){
				if(response != ''){
					$('.JSsearch_list').remove();
					$('.JSsearchContent2').append(response);
				}
			});
		} 
	}

	$('html :not(.JSsearch_list)').on("click", function() {
		$('.JSsearch_list').remove();
	});
		 
// RATING STARS
	$('.JSrev-star i').each(function(i, el){
		var val = 0;
		$(el).on('click', function(){    
			$(el).addClass('fas').removeClass('far'); 
		    $(el).prevAll('i').addClass('fas').removeClass('far'); 
			$(el).nextAll('i').removeClass('fas').addClass('far'); 
		 	if (i == 0) { val = 1; }
		 	else if(i == 1){ val = 2; }
		 	else if(i == 2){ val = 3; }
		 	else if(i == 3){ val = 4; }
		 	else if(i == 4){ val = 5; } 
		 	$('#JSreview-number').val(val); 
		}); 
	}); 
 
 // CATHEGORIES HIGH
 // 	function action_manage(){  
 // 		var action_cont = $('.JSproducts_slick'), 
 // 			lvl_1_height = $('.JSlevel-1');

	// 	if (lvl_1_height.height() > 630 && ($(window).width() >= 1024)) {
	// 		action_cont.css('margin-left', '280px'); 
	// 	} 
	// }
 // 	action_manage(); 


	$('.JStoggle-btn').on('click', function(){
		$(this).closest('div').find('.JStoggle-content').toggleClass('hidden-small'); 
	});
	 
 // RESPONSIVE NAVIGATON
 	$(".resp-nav-btn").on("click", function(){  
		$("#responsive-nav").toggleClass("open_cat");		 
	});
	$(".JSclose-nav").on("click", function(){  
		$("#responsive-nav").removeClass("open_cat");		 
	});
 
// SCROLL TO TOP 
 	$(window).scroll(function () {
        if ($(this).scrollTop() > 150) {
            $('.JSscroll-top').css('right', '20px');
        } else {
            $('.JSscroll-top').css('right', '-70px');
        }
    });
 
    $('.JSscroll-top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 600);
        return false;
    }); 
    
// filters slide down
	$(".JSfilters-slide-toggle").on('click', function(){
	    $(this).next(".JSfilters-slide-toggle-content").toggle();
	    
		$(this).toggleClass('attr-btn-opened');

	    if ($(this).children().attr('class') == 'sprite sprite-down') {
			$(this).children().removeClass('sprite sprite-down').addClass('sprite sprite-up'); 
		} else {
			$(this).children().removeClass('sprite sprite-up').addClass('sprite sprite-down'); 
		} 
	});

	if ($('.selected-filters li').length) {
		$('.JShidden-if-no-filters').show();
	} 
 
 // SELECT ARROW - FIREFOX FIX
	$('select').wrap('<span class="select-wrapper"></span>');
	 
// POPUP BANER
	if($('body').is('#start-page') && !(sessionStorage.getItem('popup')) ){   
		setTimeout(function(){ 
			$('.JSfirst-popup').animate({ top: '50%' }, 700);
		}, 1500);   

		$(document).on('click', function(e){
			var target = $(e.target);
			if(!target.is($('.popup-img'))) { 
				$('.JSfirst-popup').hide();
			} 
		}); 
		if ($('.JSfirst-popup').length) {
			sessionStorage.setItem('popup','1');
		}
	}
 
 
	if ($(window).width() > 991 ) { 

		// CATEGORIES WRAP
		$('.JSlevel-1 li').each(function () {	
			var subitems = $(this).find(".JSlevel-2 > li");
			for(var i = 0; i < subitems.length; i+=4) {
				subitems.slice(i, i+4).wrapAll("<div class='clearfix level-2-wrap'></div>");
			}
		});

	}
 

 // LEFT & RIGHT BODY LINK 
	if ($(window).width() > 1024 ) {  
		var right_link = $('.JSright-body-link'),
			left_link = $('.JSleft-body-link'),
			main = $('.JSmain'); 

		setTimeout(function(){ 
			main.before((left_link).css('top', main.position().top + 'px'));
			main.after((right_link).css('top', main.position().top + 'px'));
			left_link.width(($(window).outerWidth() - $('.container').outerWidth())/2).height(main.height()); 
			right_link.width(($(window).outerWidth() - $('.container').outerWidth())/2).height(main.height()); 

 		}, 1000); 
  	} 

// FOOTER COLS CLEARFIX
  	(function(){
  		var cols = $('.JSfooter-cols > div');
  		for(var i = 0; i < cols.length; i+=4){
  			cols.slice(i, i+4).wrapAll('<div class="clearfix JSfooter-secs"></div>');
  		}
  	}()); 

// SEARCH BY GROUP TEXT
	function selectTxt(){    
		var txt = $('.JSSearchGroup2 option:first').text(trans('Sve')); 
		// if ($(window).width() > 1024 ){ 
		// 	txt.text('Pretraga po kategorijama');  
		// }else{
		// 	txt.text('po kategorijama');  
		// }
	}
	selectTxt(); 

// OPEN SEARCH MODAL
	$('.JSopen-search-modal').on('click', function(){
		$('.JSsearch-modal').show();
	});
 
	$(document).on('click', function(e){
		if ($('.JSsearch-modal').css('display') == 'block') { 
			$('body').css('overflow', 'hidden');  

			if ( ! $('.search-content *, .JSopen-search-modal, .JSopen-search-modal *').is(e.target)) { 
				$('.JSsearch-modal').hide();
				$('body').css('overflow', '');
			} 
		} else {
			$('body').css('overflow', '');  
		}    
	});
 
 
 		// COOKIES
 	$(function(){
		function getCookie(cookieParamName){
			if(document.cookie.split('; ').find(row => row.startsWith(cookieParamName))) {
				return document.cookie
				  .split('; ')
				  .find(row => row.startsWith(cookieParamName))
				  .split('=')[1];
			}else{
				return undefined;
			}
		}
		function setCookie(cookieParamName,cookieParamValue,expireDays){
			var now = new Date();
			var time = now.getTime();
			var expireTime = time + 1000*86400*expireDays;
			now.setTime(expireTime);
			document.cookie = cookieParamName+"="+cookieParamValue+";expires="+now.toUTCString()+"; path=/;";
		}

		$("#alertCookiePolicy").hide()
		if (getCookie('acceptedCookiesPolicy') === undefined){
			//console.log('accepted policy', chk);
			$("#alertCookiePolicy").show(); 
		}
		$('#btnAcceptCookiePolicy').on('click',function(){
			//console.log('btn: accept');
			setCookie('acceptedCookiesPolicy','yes',30);
		});
		$('#btnDeclineCookiePolicy').on('click',function(){
			//console.log('btn: decline');
			setCookie('acceptedCookiesPolicy','no',30);
		});
 	});


 	// COOKIES TEXT
 	(function(){
	 	var cookiesBtn = $('.JScookiesInfo_btn');
		cookiesBtn.text('Prikaži detalje');
		cookiesBtn.on('click', function(){
			cookiesBtn.next().slideToggle('fast');
			cookiesBtn.text(function(i, text){
		   		return text == 'Prikaži detalje' ? 'Sakrij detalje' : 'Prikaži detalje'
			}); 
		});
	}());

 	// SHOW COOKIES
	setTimeout(function(){ 
	 	$('.JScookies-part').animate({ bottom: '0' }, 700);
	}, 1500);   

	// ARTICLE DETAILS GALLERY
	$('.JSproduct-gallery').each(function(i) {
        $(this).find('.img-gallery').each(function(i) {
            $(this).attr('JSgallery-index', i);
        });
    });
	
	$('.article-full-it').on('click', function() {
		var currentImgLink = $('.JSproducts-main-img.slick-active img').attr('src');
		$('.full-it-modal').show();
		$('body').addClass('full-it-preview');
		$('.full-it-img img').attr('src', currentImgLink);
	});

	if($('.JScategories-banner-flag').is(':visible')) {
		$('.product-page').remove();
	}

	$('.attr-btn').on('click', function() {
		$('.attr-drop-menu').toggle();
		$(this).toggleClass('attr-btn-opened');
		$(this).find('i').toggleClass('sprite-up');
	});

	$(document).on('click', function(e){
		var target = $(e.target);
		if(!target.is($('.attributes, .attributes *'))) { 
			$('.attr-drop-menu').hide();
			$('.attr-btn').find('i').removeClass('sprite-up');
			$('.attr-btn').removeClass('attr-btn-opened');
		} 
	}); 

	$('.attr').on('click', function() {
		var attrName = $(this).find('span').text();
		$('.JSattr-btn-name').text(attrName);

		$('.attr-drop-menu').hide();
		$('.attr-btn').find('i').removeClass('sprite-up');
		$('.attr-btn').removeClass('attr-btn-opened');
	});

	$('.product-details-tab-wrap').each(function(i) {
		var tabName = $(this).find('.product-details-tab-span'),
			tabBox = $(this).find('.product-details-tab-content');

		tabName.on('click', function() {	
			tabBox.toggle();
			tabName.find('.sprite-plus').toggleClass('sprite-minus');
		});
		if(i == 1) {
			tabBox.show();
			tabName.find('.sprite-plus').toggleClass('sprite-minus');
		}
	});

	$('.product-details-tab-wrap:first-of-type').each(function() {
		var tabName = $(this).find('.product-details-tab-span'),
			tabBox = $(this).find('.product-details-tab-content');

		tabName.find('i').toggleClass('sprite-minus');
		tabBox.toggle();
	});

	// if($('body').is('#product-page')) {
	// 	$('.product-page .JSproduct .shop-product-card').on('click', function(e) {
	// 		var product_this = $(this).parent();
	// 		$.post(base_url + 'quick-view', { roba_id: product_this.data('roba_id') }, function(response) {
	// 			product_this.find('.product-card-details').html(response);

	// 			var details = product_this.find('.product-card-details'),
	// 				cont_left = $('.container').offset().left;

	// 			details.toggle();
	// 			product_this.siblings().find('.product-card-details').hide();

	// 			var details_left = product_this.offset().left,
	// 				the_distance = Math.abs(details_left - cont_left),
	// 				container_width = $('.container').innerWidth(),
	// 				cont_right =(container_width);

	// 			details.css('margin-left', - the_distance + 10 + "px");
	// 			details.css('width', cont_right - 20);
	// 		});	
	// 	});
	// 	$('.product-page .product-card-img-link').attr('href', 'javascript:void(0);')
	// };

	$('.product-preview-close i').on('click', function() {
		$('.product-card-details').hide();
	});

	$('.size-guide').on('click', function() {
		$('.size-guide-modal').show();
		$('body').addClass('size-modal-after');
	});

	$(document).on('click', function(e){
		var target = $(e.target);
		if(!target.is($('.size-guide-modal, .size-guide-modal img, .size-guide, .size-guide *'))) { 
			$('.size-guide-modal').hide();
			$('body').removeClass('size-modal-after');
		} 
	}); 

	$(document).on('click', function(e){
		var target = $(e.target);
		if(!target.is($('.full-it-modal, .full-it-modal img, .article-full-it, .article-full-it *'))) { 
			$('.full-it-modal').hide();
			$('body').removeClass('full-it-preview');
		} 
	}); 

}); // DOCUMENT READY END
 


// ALERTIFY
function alertSuccess(message) {
	swal(message, "", "success");
		
	setTimeout(function() {
		swal.close();  
	}, 2000);
}	

function alertError(message) {
	sweetAlert(message, "", "error");
	setTimeout(function() {
		sweetAlert.close();  
	}, 2000);
}
 
document.onreadystatechange = function(){
    if (document.readyState === "complete") { 

		// $('.product-image').each(function(i, img){
		// 	if ($(img).attr('src') != 'quick_view_loader.gif') {
		// 		$(img).attr('src', '/images/no-image.jpg').addClass('img-responsive'); 
		// 	}
		// });   

		// IMAGE REPLACEMENT
		if($(window).width() > 1024) {
			$('.product-image-wrapper').on('mouseenter', function(){  

				var img = $(this).find('img'),
					placeholder = $(this).find('.JSimg-placeholder');
	 			
	 			if(img.attr('src').includes('no-image')) {
		 
					img.siblings('.JSimg-placeholder').remove();

			  	} else if (img.attr('src') != placeholder.data('src') && placeholder.length) {
 
					sessionStorage.setItem('src', img.attr('src')); 

					img.attr('src', placeholder.data('src')).hide().show();  

			  	} 

			}).on('mouseleave', function(){
				var img = $(this).find('img');
	 
	 			if (sessionStorage.getItem('src')) {
					
					img.attr('src', sessionStorage.getItem('src')).hide().show();

					sessionStorage.removeItem('src');
				}    
			});
			// IF IMAGE CLICKED
			sessionStorage.removeItem('src');
		}


		// ZOOMER
		if($(window).width() > 991) {
			if ($(".JSproduct-preview-image")[0]){
				jQuery(".JSzoom_03").elevateZoom({
					gallery:'gallery_01', 
					cursor: 'pointer',  
					scrollZoom: 'true', 
					imageCrossfade: true,  
					zoomType: 'lens',
					lensShape: 'round', 
					lensSize: 250, 
					borderSize: 1, 
					containLensZoom: true
				}); 
			} 

			if($('body').is('#artical-page')){	
				$('.zoomContainer').css({
					width: $('.zoomWrapper').width(),
					height: $('.zoomWrapper').height()
				}); 
					
				var art_img =  $('#art-img').attr('src');
				if (art_img.toLowerCase().indexOf('no-image') >= 0){
					$('.zoomContainer').hide();
				} 
			// ARTICLE ZOOMER		
				$('.zoomContainer').each(function(i, el){
					if ( i != 0) {
						$(el).remove();
					}
				});
			}  
		}

		if ($('.JSproduct-main-slick').length){ 
			$('.JSproduct-main-slick').slick({ 
				autoplay: false,
				slidesToShow: 1,
				draggable: false,
				// asNavFor: $('.JSproduct-gallery-slick'),
				slidesToScroll: 1
			}); 
		}

		$('.JSproduct-gallery .img-gallery').on('click', function() {
			clickedGalleryIndex = $(this).attr('JSgallery-index');
			$('.JSproduct-main-slick').slickGoTo(clickedGalleryIndex);
		});
		
	} 
}

 